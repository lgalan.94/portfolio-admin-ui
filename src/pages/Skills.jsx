import { Layout, CustomButton, InvisibleButton, CustomDialogBox } from '../components';
import { useState, useEffect } from 'react';
import { TiChevronLeft } from "react-icons/ti";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { Input, Alert } from '@material-tailwind/react';
import { BiSolidError } from "react-icons/bi";

const Skills = () => {
	const [items, setItems] = useState([]);
	let [isLoading, setIsLoading] = useState(true);
	let [isClicked, setIsClicked] = useState(true);
	let handleCollapse = () => setIsClicked(!isClicked);
	let [isSaveButtonClicked, setIsSaveButtonClicked] = useState(false);
	let [name, setName] = useState('');
	let [openAlert, setOpenAlert] = useState(false);
	let [inAlertName, setInAlertName] = useState('');

	let fetchSkills = () => {
				fetch(`${import.meta.env.VITE_API_URL}/skills`, {
						method: 'GET',
						headers: {
								'Content-Type': 'application/json'
						}
				})
				.then(response => response.json())
				.then(data => {
							if (data !== 0) {
										setItems(data.map(item => {
													return (
																	<Skill key={item._id} itemProps={item} />
														)
										}))
										setIsLoading(false);
							} else {
										setItems(<div>No item in the database!</div>);
										setIsLoading(false);
							}
				})
				.catch(error => console.error)
	}

	let refresh = () => {
				setIsLoading(true);
				fetchSkills();
				setTimeout(() => setIsLoading(false), 5000)
	}

	useEffect(() => {
			fetchSkills();
			refresh();
	}, [])

let handleAdd = (e) => {
			e.preventDefault();
			setIsSaveButtonClicked(true);
			fetch(`${import.meta.env.VITE_API_URL}/skills/add-skill`, {
						method: 'POST',
						headers: {
								'Content-Type': 'application/json',
								'Authorization': `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
									name: name.toUpperCase()
						})
			})
			.then(response => response.json())
			.then(data => {
						if (data === true) {
									toast.success('New data successfully added!');
									fetchSkills();
									setIsSaveButtonClicked(false);
									setName('');		
						} else if (data.name === name) {
									setOpenAlert(true);
									setInAlertName(data.name);
									setIsSaveButtonClicked(false);
									setTimeout(() => setOpenAlert(false), 6000)
						} else if (!data.isAdmin) {
									toast.info("Current user role is view-mode only!")
									setTimeout(() => toast.info("Login as admin to add new data!"), 1500);
									setName("");
									setIsSaveButtonClicked(false);
						} else {
									toast.error('Unable to add new data!');
									setIsSaveButtonClicked(false);
									setName('');
						}
			})
			.catch(error => console.error)
}

	/************Props************/
	const Skill = (props) => {
		const { _id, name } = props.itemProps;
		let [isButtonDeleteClicked, setIsButtonDeleteClicked] = useState(false);
		let [openDialog, setIsOpenDialog] = useState(false);
		let handleOpenDialog = () => setIsOpenDialog(!openDialog);

		const handleDelete = (e) => {
					e.preventDefault();
					setIsButtonDeleteClicked(true);
					fetch(`${import.meta.env.VITE_API_URL}/skills/remove/${_id}`, {
								method: 'DELETE',
								headers: {
									'Authorization': `Bearer ${localStorage.getItem('token')}`
								}
					})
					.then(response => response.json())
					.then(data => {
								if (data === true) {
											toast.success('Successfully Deleted!');
											fetchSkills();
											setIsOpenDialog(false);
											setIsButtonDeleteClicked(false);
								} else {
											toast.error('Unable to remove data!');
											setIsOpenDialog(false);
											setIsButtonDeleteClicked(false);
								}
					})
					.catch(error => console.error)
		}

			return (
					<>
							<div className="text-center px-6 pt-3 uppercase border border-1 border-dark/25 rounded-md group hover:scale-105 shadow-lg"> 
										<span>{name}</span>
										<InvisibleButton
												icon={<RiDeleteBin3Fill />}
												label="delete"
												tooltip="!bg-red-500"
												placement="bottom"
												btn="!text-red-500"
												handleClick={handleOpenDialog}
										/>
							</div>
		    	<CustomDialogBox
		    			title="Confirmation"
		    			open={openDialog}
		    			handleOpen={handleOpenDialog}
		    	>
		    				Delete {name}?
		    			
		    			<div className="mt-10 flex flex-row justify-center gap-1">
		    					<CustomButton
		    							label="cancel"
		    							handleClick={handleOpenDialog}
		    							tooltip="hidden"
		    					/>
		    					{
		    							!isButtonDeleteClicked ? (
		    											<CustomButton
		    													label="delete"
		    													color="red"
		    													handleClick={handleDelete}
		    													tooltip="hidden"
		    											/>
		    								) : (
		    										<CustomButton
		    												label="deleting..."
		    												color="red"
		    												isDisabled={true}
		    												tooltip="hidden"
		    										/>
		    								)
		    					}
		    			</div>
		    </CustomDialogBox>
					</>
			)
	}
	/*****************************/
			return (
						<Layout
								pageTitle="Skills"
								loading={
												isLoading ? '' : 'hidden'
								}
								childrenClass={
											isLoading ? 'hidden' : ''
								}
								component3={
											<CustomButton
													tooltip="hidden"
													variant="text"
													color=""
													icon={<TiChevronLeft className={`w-4 h-4 ${isClicked ? 'rotate-180 transition-transform delay-75' : ''}`} />}
													handleClick={handleCollapse}
											/>
								}
								card2={
											isClicked ? '' : 'hidden'
								}
								component4="Add New"
								children2={
											<div className="flex flex-col gap-3">
													<Input
															label="SKILL NAME"
															color="teal"
															value={name.toUpperCase()}
															onChange={(e) => setName(e.target.value)}
													/>
														<div className="flex flex-row justify-center gap-1 mt-6">
																{
																			!isSaveButtonClicked ? (
																						<CustomButton
																								label="Add"
																								color="green"
																								handleClick={handleAdd}
																								tooltip="hidden"
																						/>
																				) : (
																						<CustomButton
																								label="Adding..."
																								color="green"
																								isDisabled={true}
																								icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																								tooltip="hidden"
																						/>
																				)
																}
																<CustomButton
																		label="Clear"
																		handleClick={() => setName('')}
																		tooltip="hidden"
																/>
														</div>
											</div>
								}
								copyRight={
											isClicked ? 'hidden' : ''
								}
								itemNumber={
												items.length > 0 ? `${items.length} items` : 'No Data'
								}
								refresh={refresh}
						>

								<div className="w-full"> 
										<Alert
														icon={<BiSolidError className="w-5 h-5" />}
														size="sm"
								      animate={{
								      	mount: { y: 0 },
								      	unmount : {y: 1},
								      }}
								      open={openAlert}
								      className="rounded-md mb-2 p-2 border-l-4 border-red-500 bg-red-500/20 text-sm text-red-500"
								    >
								      	{inAlertName.toUpperCase()} already exists!
								    </Alert>
								</div>

								<div className="flex flex-row justify-center mx-auto gap-4 flex-wrap">
									 {items.length > 0 ? items : "No data"}
								</div>
						</Layout>
			)
}

export default Skills