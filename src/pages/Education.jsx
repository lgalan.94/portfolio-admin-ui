import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Layout, CustomButton, InvisibleButton, CustomDialogBox, CustomDrawer } from '../components';
import { TiChevronLeft } from "react-icons/ti";
import { RiRefreshFill } from "react-icons/ri";
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { FaRegCheckCircle } from "react-icons/fa";
import { useState, useEffect } from 'react';
import { Accordion, AccordionHeader, AccordionBody, Typography, Input, Textarea, Alert, Drawer } from '@material-tailwind/react';


const Education = () => {

	let token = localStorage.getItem('token');

	const [items, setItems] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [isCollapsed, setIsCollapsed] = useState(false);
	const handleCollapse = () => setIsCollapsed(!isCollapsed);
	const [isSave, setIsSave] = useState(false);
	const [title, setTitle] = useState('');
	const [school, setSchool] = useState('');
	const [schoolLink, setSchoolLink] = useState('');
	const [timeRange, setTimeRange] = useState('');
	const [address, setAddress] = useState('');
	const [learnings, setLearnings] = useState('');
	const [successAlert, setSuccessAlert] = useState(false);
	let [updateAlert, setUpdateAlert] = useState(false);

	/**************Fetch Data**************/
	const fetchEducation = () => {
		 		fetch(`${import.meta.env.VITE_API_URL}/education`, {
		 				method: 'GET',
		 				headers: {
		 						'Content-type': 'application/json'
		 				}
		 		})
		 		.then(response => response.json())
		 		.then(data => {
		 						if (data.length > 0) {
		 									setItems(data.map(item => {
		 											return (
		 														<CustomCard key={item._id} itemProps={item} />
		 											)
		 									}))
		 									setIsLoading(false);
		 						} else {
		 									setItems(<div>No data in the database!</div>)
		 									setIsLoading(false);
		 						}
		 		})
		 		.catch(error => console.error) 
		} 

	const refresh = () => {
			setIsLoading(true);
			fetchEducation();
			setTimeout(() => setIsLoading(false), 4000)
	}

		useEffect(() => {
					fetchEducation();
					refresh();
		}, [])

		const clear = () => {
			setIsSave(false);
			setTitle('');
			setSchool('');
			setSchoolLink('');
			setTimeRange('');
			setAddress('');
			setLearnings('');
		}

		const handleAdd = (event) => {
		  event.preventDefault();
		  setIsSave(true);
		  fetch(`${import.meta.env.VITE_API_URL}/education/add`, {
		    method: 'POST',
		    headers: {
		      'Content-Type': 'application/json',
		      'Authorization': `Bearer ${token}`
		    },
		    body: JSON.stringify({
		      title: title,
		      school: school,
		      schoolLink: schoolLink,
		      timeRange: timeRange,
		      address: address,
		      learnings: learnings,
		    })
		  })
		    .then(result => result.json())
		    .then(data => {
		      	if (data === true) {
		      	  setSuccessAlert(true);
		      	  fetchEducation();
		      	 	clear();
		      	 	setTimeout(() => setSuccessAlert(false), 4500);
		      	} else if (!data.isAdmin) {
		      			toast.info("Current user role is view-mode only!")
		      			setTimeout(() => toast.info("Login as admin to add new data!"), 1500);
		      			clear();
		      	} else {
		      	  toast.error('Error adding new data!');
		      	  setTimeout(() => 1000);
		      	  clear();
		      	}
		    })
		    .catch(error => console.error);
		};

	/**************************************/

	/***********Card Props*************/
	const CustomCard = (props) => {
		const { _id, title, school, schoolLink, timeRange, address, learnings } = props.itemProps;
		const [isAccordionOpen, setIsAccordionOpen] = useState(0);
		const handleAccordionOpen = (value) => setIsAccordionOpen(isAccordionOpen === value ? 0 : value);
		const [openDialog, setIsOpenDialog] = useState(false);
		const handleOpenDialog = () => setIsOpenDialog(!openDialog);
		const [isClicked, setIsClicked] = useState(false);

		let [newTitle, setNewTitle] = useState(title);
		let [newSchool, setNewSchool] = useState(school);
		let [newSchoolLink, setNewSchoolLink] = useState(schoolLink);
		let [newTimeRange, setNewTimeRange] = useState(timeRange);
		let [newAddress, setNewAddress] = useState(address);
		let [newLearnings, setNewLearnings] = useState(learnings);
		let [isUpdateClicked, setIsUpdateClicked] = useState(false);

		const [openDrawer, setOpenDrawer] = useState(false);

		const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
		const handleCloseDrawer = () => setOpenDrawer(false);
		

		const handleDelete = (e) => {
					e.preventDefault();
					setIsClicked(true);
					fetch(`${import.meta.env.VITE_API_URL}/education/${_id}`, {
								method: 'DELETE',
								headers: {
										'Authorization': `Bearer ${token}`
								}
					})
					.then(response => response.json())
					.then(data => {
									if (data === true) {
												toast.success('Successfully Deleted!');
												fetchEducation();
												setIsOpenDialog(false);
												setIsClicked(false);
									} else {
												toast.error('Unable to remove data!');
												setIsOpenDialog(false);
												setIsClicked(false);
									}
					})
					.catch(error => console.error)
		}

		const handleUpdate = (e) => {
					e.preventDefault();
					setIsUpdateClicked(true);
					fetch(`${import.meta.env.VITE_API_URL}/education/${_id}`, {
								method: "PATCH",
								headers: {
										'Content-type': 'application/json',
										'Authorization': `Bearer ${token}`
								},
								body: JSON.stringify({
										newTitle,
										newSchool,
										newSchoolLink,
										newTimeRange,
										newAddress,
										newLearnings
								})
					})
					.then(response => response.json())
					.then(data => {
								if (data === true) {
											setUpdateAlert(true);
											fetchEducation();
											setIsUpdateClicked(false);
											setOpenDrawer(false);
											setTimeout(() => setUpdateAlert(false), 4500)
								} else {
											toast.error('Unable to update data!');
											setOpenDrawer(false);
								}
					})
					.catch(error => console.error)
		}

			return (
					<>
						<CustomDrawer
									open={openDrawer}
									openDrawer={handleOpenDrawer}
									closeDrawer={handleCloseDrawer}
							>
									<div className="flex flex-col font-ibm gap-3">
											<div className="flex flex-row gap-3 px-10">
													<Input
															label="TITLE"
															className="capitalize"
															color="teal"
															value={newTitle}
															onChange={(e) => setNewTitle(e.target.value)}
													/>
													<Input
															label="YEAR RANGE"
															color="teal"
															value={newTimeRange}
															onChange={(e) => setNewTimeRange(e.target.value)}
													/>
													<Input
															label="SCHOOL"
															color="teal"
															value={newSchool}
															onChange={(e) => setNewSchool(e.target.value)}
													/>
											</div>
											
											<div className="flex flex-row gap-3 px-10">
													<div className="w-full">	
														<Textarea
																label="SCHOOL LINK"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={newSchoolLink}
																onChange={(e) => setNewSchoolLink(e.target.value)}
														/>
													</div>
													
													<div className="w-full">	
														<Textarea
																label="ADDRESS"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={newAddress}
																onChange={(e) => setNewAddress(e.target.value)}
														/>
													</div>
											</div>
											
											<div className="w-full px-10">	
												<Textarea
														label="LEARNINGS"
														className="h-full min-h-[50px] scrollbar-thin"
														color="teal"
														value={newLearnings}
														onChange={(e) => setNewLearnings(e.target.value)}
												/>
											</div>
												<div className="flex flex-row justify-center gap-1 mt-4">
														{
																	!isUpdateClicked ? (
																				<CustomButton
																						label="Update"
																						color="green"
																						tooltip="hidden"
																						handleClick={handleUpdate}
																				/>
																		) : (
																				<CustomButton
																						label="Updating..."
																						color="green"
																						isDisabled={true}
																						icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																						tooltip="hidden"
																				/>
																		)
														}
														<CustomButton
																label="Clear"
																tooltip="hidden"
														/>
												</div>
									</div>
							</CustomDrawer>
		    <Accordion open={isAccordionOpen === 1} className="rounded-lg border border-blue-gray-300 w-full w-full max-w-[20rem] group px-4">
        <AccordionHeader
          onClick={() => handleAccordionOpen(1)}
          className={`border-b-0 transition-colors font-ibm capitalize ${
            isAccordionOpen === 1 ? "text-green-500 hover:!text-green-700" : ""
          }`}
        >
          {title}
          <InvisibleButton
          		icon={<MdRebaseEdit />}
          		label="edit"
          		tooltip="!bg-green-500"
          		placement="top"
          		btn="!text-green-500 absolute right-12"
          		handleClick={handleOpenDrawer}
          />
          <InvisibleButton
          		icon={<RiDeleteBin3Fill />}
          		label="delete"
          		tooltip="!bg-red-500"
          		placement="top"
          		btn="!text-red-500 absolute right-2"
          		handleClick={handleOpenDialog}
          />
        </AccordionHeader>
        <AccordionBody className="pt-0 font-ibm text-sm font-normal">
							    <Typography className="text-sm">
							      {school}
							    </Typography>
							    <Typography className="text-sm">
							      {schoolLink}
							    </Typography>
							    <Typography className="text-sm">
							      {timeRange}
							    </Typography>
							    <Typography className="text-sm">
							      {address}
							    </Typography>
							    <Typography className="text-sm">
							      {learnings}
							    </Typography>
        </AccordionBody>
      </Accordion>
	    	<CustomDialogBox
	    			title="Confirmation"
	    			open={openDialog}
	    			handleOpen={handleOpenDialog}
	    	>
	    				Delete {title}?
	    			
	    			<div className="mt-10 flex flex-row justify-center gap-1">
	    					<CustomButton
	    							label="cancel"
	    							handleClick={handleOpenDialog}
	    							tooltip="hidden"
	    					/>
	    					{
	    							!isClicked ? (
 													<CustomButton
 															label="delete"
 															color="red"
 															handleClick={handleDelete}
 															tooltip="hidden"
 													/>
	    								):(
	    									<CustomButton
	    											label="deleting..."
	    											color="red"
	    											isDisabled={true}
	    											tooltip="hidden"
	    									/>
	    							)
	    					}
	    			</div>
	    </CustomDialogBox>
	    </>
				)
		}
	
	/**********************************/
			return (
						<Layout
								loading={
											isLoading ? '' : 'hidden'
								}
								childrenClass={
											isLoading ? 'hidden' : ''
								}
								refresh={refresh}
								pageTitle="education"
								component3={
											<CustomButton
													tooltip="hidden"
													variant="text"
													color=""
													icon={<TiChevronLeft className={`w-4 h-4 ${!isCollapsed ? 'rotate-180 transition-transform delay-75' : ''}`} />}
													handleClick={handleCollapse}
											/>
								}
								component4="Add New"
								card2={
											isCollapsed ? 'hidden' : '!min-w-[18rem]'
								}
								copyRight={
											!isCollapsed ? 'hidden' : ''
								}
								itemNumber={
												items.length > 0 ? `${items.length} items` : 'No Data'
								}
								children2={
											<div className="flex flex-col font-ibm gap-3">
													<Input
															label="TITLE"
															className="capitalize"
															color="teal"
															value={title}
															onChange={(e) => setTitle(e.target.value)}
													/>
													<Input
															label="YEAR RANGE"
															color="teal"
															value={timeRange}
															onChange={(e) => setTimeRange(e.target.value)}
													/>

													<div className="">	
														<Textarea
																label="SCHOOL"
																className="h-full min-h-[60px] scrollbar-thin"
																color="teal"
																value={school}
																onChange={(e) => setSchool(e.target.value)}
														/>
													</div>
													
													<div className="-mt-2">	
														<Textarea
																label="SCHOOL LINK"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={schoolLink}
																onChange={(e) => setSchoolLink(e.target.value)}
														/>
													</div>
													
													<div className="-mt-2">	
														<Textarea
																label="ADDRESS"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={address}
																onChange={(e) => setAddress(e.target.value)}
														/>
													</div>
													<div className="-mt-2">	
														<Textarea
																label="LEARNINGS"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={learnings}
																onChange={(e) => setLearnings(e.target.value)}
														/>
													</div>
														<div className="flex flex-row justify-center gap-1 mt-6">
																{
																			!isSave ? (
																						<CustomButton
																								label="Save"
																								color="green"
																								tooltip="hidden"
																								handleClick={handleAdd}
																						/>
																				) : (
																						<CustomButton
																								label="Saving..."
																								color="green"
																								isDisabled={true}
																								icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																								tooltip="hidden"
																						/>
																				)
																}
																<CustomButton
																		label="Clear"
																		tooltip="hidden"
																/>
														</div>
											</div>
								}
						>	
								<div className="w-full"> 
										<Alert
														size="sm"
								      icon={<FaRegCheckCircle className="w-4 h-4" />}
								      animate={{
								      	mount: { y: 0 },
								      	unmount : {y: 1},
								      }}
								      open={successAlert}
								      className="rounded-md mb-2 p-2 border-l-4 border-[#2ec946] bg-[#2ec946]/10 text-sm text-[#2ec946]"
								    >
								      New data added!
								    </Alert>
								</div>
								<div className="w-full"> 
										<Alert
														size="sm"
								      icon={<FaRegCheckCircle className="w-4 h-4" />}
								      animate={{
								      	mount: { y: 0 },
								      	unmount : {y: 1},
								      }}
								      open={updateAlert}
								      className="rounded-md mb-2 p-2 border-l-4 border-[#2ec946] bg-[#2ec946]/10 text-sm text-[#2ec946]"
								    >
								      Successfully updated!
								    </Alert>
								</div>
								<div className="flex flex-row justify-center flex-wrap gap-3">
											{items}
								</div>
						</Layout>
			)
}

export default Education