import { Layout, InvisibleButton, CustomButton, CustomDialogBox, CustomDrawer } from '../components';
import { useState, useEffect } from 'react';
import { Accordion, AccordionHeader, AccordionBody, Input, Textarea } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { TiChevronLeft } from "react-icons/ti";

/********certificates*********/
//https://drive.google.com/uc?export=view&id
let backend = 'https://drive.google.com/uc?export=view&id=15XGRw1S6uzL-A2Kgt_4aHMnzQRsqpBxH';
let bootcamp_completion = "https://drive.google.com/uc?export=view&id=1uFZAsnjvUT_Kp7owpxtvisG2ArWbE5Gv";
let ca_talk_1 = "https://drive.google.com/uc?export=view&id=1rV1OKxIrjYD6Ngf-x3-TqwrQ8VIQJQe1";
let ca_talk_2 = "https://drive.google.com/uc?export=view&id=1B-K9OuUysLgDUaeH73Dhhsu8xVX2ngUW";
/*****************************/

const Certificates = () => {

		let token = localStorage.getItem('token');
		const [items, setItems] = useState([]);
		const [isLoading, setIsLoading] = useState(true);
		let [isCard2Open, setIsCard2Open] = useState(false);
		const handleCardOpen = () => setIsCard2Open(!isCard2Open);
		const [title, setTitle] = useState('');
		const [imgUrl, setImgUrl] = useState('');
		const [isAddButtonClicked, setIsAddButtonClicked] = useState(false);

		const handleClear = () => {
					setTitle('');
					setImgUrl('');
					setIsAddButtonClicked(false);
		}

		const fetchCertificates = () => {
					fetch(`${import.meta.env.VITE_API_URL}/certificates/`, {
								method: 'GET',
								headers: {
									'Content-Type': 'application/json'
								}
					})
					.then(response => response.json())
					.then(data => {
								data.sort();
								if (data.length > 0) {
											setItems(data);
											setIsLoading(false);
								} else {
										setItems(<div>No data!</div>)
										setIsLoading(false);
								}
					})
					.catch(error => console.error)
		}

		const refresh = () => {
					setIsLoading(true);
					fetchCertificates();
					setTimeout(() => setIsLoading(false), 5000);
					handleClear();
		}

		useEffect(() => {
				fetchCertificates();
				refresh();
		}, [])

		const handleAdd = (e) => {
				e.preventDefault();
				setIsAddButtonClicked(true);
				fetch(`${import.meta.env.VITE_API_URL}/certificates/add`, {
						method: 'POST',
						headers: {
								'Content-Type': 'application/json',
								'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify({
									title: title,
									imgUrl: imgUrl
						})
				})
				.then(response => response.json())
				.then(data => {
								if (data === true) {
											toast.success('New data successfully added!');
											handleClear();
											fetchCertificates();
								} else if (!data.isAdmin) {
										toast.info("Current user role is view-mode only!")
										setTimeout(() => toast.info("Login as admin to add new data!"), 1500);
										handleClear();
								} else {
										toast.error('Error adding new data!');
										handleClear();
								}
				})
				.catch(error => console.error);
		}

		/******************************************/
			let CustomAccordion = (props) => {

				const { _id, title, imgUrl } = props.itemProps;
				let [open, setOpen] = useState(0);
				let [isDeleteButtonClicked, setIsDeleteButtonClicked] = useState(false);
				let [openDialog, setOpenDialog] = useState(false);
				let [isUpdateClicked, setIsUpdateClicked] = useState(false);
				const [openDrawer, setOpenDrawer] = useState(false);

				const [newTitle, setNewTitle] = useState(title, (prevTitle) => prevTitle); // Access previous key
				const [newImgUrl, setNewImgUrl] = useState(imgUrl, (prevImgUrl) => prevImgUrl); // Access previous value

				const handleTitleChange = (e) => {
							setNewTitle((prevTitle) => e.target.value); 
				}

				const handleImgUrlChange = (e) => {
							setNewImgUrl((prevImgUrl) => e.target.value); 
				}

				const handleOpenDialog = () => setOpenDialog(!openDialog);
				const handleOpen = (value) => setOpen(open === value ? 0 : value);
			

				const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
				const handleCloseDrawer = () => setOpenDrawer(false);

				const handleDelete = (e) => {
						e.preventDefault();
						setIsDeleteButtonClicked(true);
						fetch(`${import.meta.env.VITE_API_URL}/certificates/${_id}`, {
										method: 'DELETE',
										headers: {
													'Authorization': `Bearer ${token}`
										}
						})
						.then(response => response.json())
						.then(data => {
									if (data === true) {
												toast.success('Successfully deleted!');
												setOpenDialog(false);
												fetchCertificates();
									} else {
											toast.error('Error deleting data!')
											setOpenDialog(false);
									}
						})
						.catch(error => console.error)
				}

				const handleUpdate = (e) => {
							e.preventDefault();
							setIsUpdateClicked(true)
							fetch(`${import.meta.env.VITE_API_URL}/certificates/${_id}`, {
										method: 'PATCH',
										headers: {
												'Content-Type': 'application/json',
												'Authorization': `Bearer ${token}`
										},
										body: JSON.stringify({
												newTitle,
												newImgUrl
										})
							})
							.then(response => response.json())
							.then(data => {
								console.log(data)
										if (data === true) {
														toast.success('Successfully Updated!');
														fetchCertificates();
														refresh();
														setOpenDrawer(false);
														setIsUpdateClicked(false);
										} else {
														toast.error('Error updating data!');
														setOpenDrawer(false);
														setIsUpdateClicked(false);
										}
							})
							.catch(error => console.error)
				}

						return (
							<>
									<CustomDrawer
											open={openDrawer}
											openDrawer={handleOpenDrawer}
											closeDrawer={handleCloseDrawer}
									>
											<form className="flex self-center flex-row gap-4 p-10">
											  <div className="w-2/5">
											  		<Input
											  				label="TITLE" 
											  				value={newTitle}
											  				onChange={handleTitleChange}
											  				color="teal"
											  		/>
											  </div>
											  <div className="w-3/5">
											  		<Textarea
											  				label="Img Url" 
											  				value={newImgUrl}
											  				onChange={handleImgUrlChange}
											  				color="teal"
											  		/>
											  </div>
											</form>
											<div className="flex flex-row mt-4 gap-2 justify-center">
													<CustomButton 
															label="Cancel"
															handleClick={handleCloseDrawer}
													/>
													{
															!isUpdateClicked ? (
																		<CustomButton 
																				label="Update"
																				handleClick={handleUpdate}
																				color="green"
																		/>
																) : (
																		<CustomButton 
																				label="Updating..."
																				icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																				color="green"
																				isDisabled={true}
																		/>
																)
													}
											</div>
									</CustomDrawer>
					    <Accordion open={open === 1} className="rounded-lg border border-blue-gray-300 w-full group px-4">
			        <AccordionHeader
			          onClick={() => handleOpen(1)}
			          className={`border-b-0 capitalize transition-colors font-ibm  ${
			            open === 1 ? "text-green-500 hover:!text-green-700" : ""
			          }`}
			        >
			          {title}
			          <InvisibleButton
			          		icon={<MdRebaseEdit />}
			          		label="edit"
			          		tooltip="!bg-green-500"
			          		placement="top"
			          		btn="!text-green-500 absolute right-12"
			          		handleClick={handleOpenDrawer}
			          />
			          <InvisibleButton
			          		icon={<RiDeleteBin3Fill />}
			          		label="delete"
			          		tooltip="!bg-red-500"
			          		placement="top"
			          		btn="!text-red-500 absolute right-2"
			          		handleClick={handleOpenDialog}
			          />
			        </AccordionHeader>
			        <AccordionBody className="pt-0 font-ibm text-sm font-normal">
			      					<img src={imgUrl} className="" alt="image" />
			        </AccordionBody>
			      </Accordion>

 			    	<CustomDialogBox
 			    			title="Confirmation"
 			    			open={openDialog}
 			    			handleOpen={handleOpenDialog}
 			    	>
 			    				Delete {title}?
 			    			
 			    			<div className="mt-10 flex flex-row justify-center gap-1">
 			    					<CustomButton
 			    							label="cancel"
 			    							handleClick={handleOpenDialog}
 			    							tooltip="hidden"
 			    					/>
 			    					{
 			    							!isDeleteButtonClicked ? (
    													<CustomButton
    															label="delete"
    															color="red"
    															handleClick={handleDelete}
    															tooltip="hidden"
    													/>
 			    								):(
 			    									<CustomButton
 			    											label="deleting..."
 			    											color="red"
 			    											isDisabled={true}
 			    											tooltip="hidden"
 			    									/>
 			    							)
 			    					}
 			    			</div>

 			    </CustomDialogBox>
				  </>
						)
			}
		/***Layout props*****/
			const addNew = (
							<div className="flex flex-col gap-3">
									<Input
											label="TITLE"
											className="capitalize"
											color="teal"
											value={title}
											onChange={(e) => setTitle(e.target.value)}
									/>
									<Textarea
											label="GDrive Img Url"
											color="teal"
											value={imgUrl}
											onChange={(e) => setImgUrl(e.target.value)}
									/>
										<div className="flex flex-row justify-center gap-1 mt-6">
												{
															!isAddButtonClicked ? (
																		<CustomButton
																				label="Save"
																				color="green"
																				handleClick={handleAdd}
																				tooltip="hidden"
																		/>
																) : (
																		<CustomButton
																				label="Saving..."
																				color="green"
																				isDisabled={true}
																				icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																				tooltip="hidden"
																		/>
																)
												}
												<CustomButton
														label="Clear"
														handleClick={handleClear}
														tooltip="hidden"
												/>
										</div>
							</div>
				)
		/*********/

			return (
						<Layout
							refresh={refresh}
							pageTitle="Certificates"
							loading={
									isLoading ? '' : 'hidden'
							}
							childrenClass={
									isLoading ? 'hidden' : ''
							}
							card2={
									isCard2Open ? '' : 'hidden'
							}
							component3={
										<CustomButton
												tooltip="hidden"
												variant="text"
												color=""
												icon={<TiChevronLeft className={`w-4 h-4 ${isCard2Open ? 'rotate-180 transition-transform delay-75' : ''}`} />}
												handleClick={handleCardOpen}
										/>
							}
							children2={addNew}
							copyRight={
										isCard2Open ? 'hidden' : ''
							}
							itemNumber={
											items.length > 0 ? `${items.length} items` : 'No Data'
							}
						>
								<div className="grid grid-cols-2 gap-2">

										{
											items.length > 0 ? (
															items.map((item) => {
																		return (
																						<CustomAccordion 
																								key={item._id} 
																								itemProps={item} 
																						/>
																			)
															})
												) : (
														"Loading ..."
												)
										}
								</div>
						</Layout>
			)
}

export default Certificates