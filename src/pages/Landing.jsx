import { Card, CardBody, Typography, Button, Input, Alert } from '@material-tailwind/react';
import { IoLogInSharp } from "react-icons/io5";
import { IoMdClose } from "react-icons/io";
import { useState, useContext, useEffect } from 'react';
import { ImSpinner2 } from "react-icons/im";
import { CustomButton } from '../components';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DataContext from '../DataContext';
import { useNavigate } from 'react-router-dom';

const Landing = () => {

		let navigate = useNavigate();

		const { data, setData, settings } = useContext(DataContext);
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [open, setOpen] = useState(false);
		const [isClicked, setIsClicked] = useState(false);
		const [loginAlert, setLoginAlert] = useState(false);
		const [redirectAlert, setRedirectAlert] = useState(false);
		const [isLoading, setIsLoading] = useState(true);

		const handleOpen = () => setOpen(!open);

		const retrieveUserDetails = (token) => {
		  fetch(`${import.meta.env.VITE_API_URL}/user/user-details`, {
		    method: 'GET',
		    headers: {
		      Authorization: `Bearer ${token}`
		    }
		  })
		    .then(result => result.json())
		    .then(resp => {
		      setData({
		        id: resp._id,
		        isAdmin: resp.isAdmin
		      });

		      setLoginAlert(true);
		      setTimeout(() => setRedirectAlert(true), 2000);
		      setTimeout(() => navigate('/home'), 5000);
		      
		    });
		}; 


		const handleLogin = (e) => {
		  e.preventDefault();
		  setIsClicked(true);
		  fetch(`${import.meta.env.VITE_API_URL}/user/login`, {
		    method: 'POST',
		    headers: {
		      'Content-type': 'application/json'
		    },
		    body: JSON.stringify({
		      email: email,
		      password: password
		    })
		  })
		    .then(result => result.json())
		    .then(data => {
		      if (data === false) {
		        toast.error('Incorrect email or password!');
		        setTimeout(() => 1500);
		        setTimeout(() => setIsClicked(false), 1000);
		      } else {
		        localStorage.setItem('token', data.auth);
		        retrieveUserDetails(data.auth);
		        toast.success("Login Successful!");
		        setTimeout(() => setIsClicked(false), 1000);
		      }
		    });
		};

		function Icon() {
		  return (
		    <svg
		      xmlns="http://www.w3.org/2000/svg"
		      viewBox="0 0 24 24"
		      fill="currentColor"
		      className="h-4 w-4"
		    >
		      <path
		        fillRule="evenodd"
		        d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
		        clipRule="evenodd"
		      />
		    </svg>
		  );
		}

		let loading = (
				<div className="w-full h-full min-h-screen min-w-screen flex flex-col justify-center items-start">
					Loading ...
				</div>
			)

		useEffect(() => {
					if (settings.name !== null && settings.banner !== null) {
								setIsLoading(false);
					}
		}, [settings])

			return (
						<div className="flex flex-row">
								<Card className="shadow-none px-10 w-full min-w-[70vw]" color="transparent">
										<CardBody className="w-full h-full min-h-screen min-w-screen flex flex-col justify-center items-start">
												<Typography className="w-full max-w-[40rem] font-ibm font-bold tracking-wide transition-transform delay-75 uppercase" variant="h1">{ isLoading ? loading : settings.name }</Typography>
												<Typography className="font-ibm font-normal italic" variant="h5">{ isLoading ? "" : settings.banner }</Typography>
												
												{
														isLoading ? "" : data.id === undefined || data.id === null ? (
																		<Button onClick={handleOpen} className="flex mt-8 flex-row capitalize hover:scale-105 items-center transition-colors delay-75" variant="filled" color={open ? "" : "green"}> {open ? "" : "Sign In" } &nbsp; { open ? <IoMdClose className="w-4 h-4" /> : <IoLogInSharp className="w-4 h-4" />} </Button>
															) : (
																		<Button onClick={() => navigate('/home')} className="flex mt-8 flex-row capitalize hover:scale-105 items-center transition-colors delay-75" variant="filled" color="blue"> Dashboard </Button>
															)
												}

										</CardBody>
								</Card>

								<Card className={`rounded-r-none px-5 transition-slide delay-700 w-full max-w-[22rem] min-w-[22rem] ${open ? "-translateX(100%) duration-700" : "translateX(0) hidden"} `} color="">
										<CardBody className="w-full h-full min-h-screen min-w-screen flex flex-col justify-center items-start">
														
														<Alert
												      icon={<Icon />}
												      animate={{
												      	mount: { y: 0 },
												      	unmount : {y: 1},
												      }}
												      open={loginAlert}
												      className="mb-2 text-xs border-l-4 border-[#2ec946] bg-[#2ec946]/10 font-medium text-[#2ec946]"
												    >
												      Logged in as {data.isAdmin ? "admin!" : "guest!"}
												    </Alert>
												    		<Alert
												          icon={<Icon />}
												          animate={{
												          	mount: { y: 0 },
												          	unmount : {y: 1},
												          }}
												          open={redirectAlert}
												          className="mb-10 text-xs border-l-4 border-[#2ec946] bg-[#2ec946]/10 font-medium text-[#2ec946]"
												        >
												          redirecting...
												        </Alert>

														<Typography variant="h3" className="mx-auto mb-5 uppercase">Login</Typography>

																			<form className="w-full justify-center">

																					

																			<div className="mt-2 flex flex-col gap-2">
																	    
																	    <div className="w-full">
																	    		<Input 
																	    				type="text"
																	    				label="Email" 
																	    				size="lg" 
																	    				color="teal"
																	    				value={email}
																	    				onChange={(e) => setEmail(e.target.value)}
																	    		/>
																	    </div>
																	    <div>
																	    		<Input 
																	    				label="Password"
																	    				type="password" 
																	    				size="lg" 
																	    				color="teal"
																	    				value={password}
																	    				onChange={(e) => setPassword(e.target.value)}
																	    		/>
																	    </div>

																	    <div className="flex w-full justify-center flex-row gap-1">
																	    		<Button onClick={handleOpen} className="flex mt-8 flex-row capitalize hover:scale-105 items-center" variant="filled"> Cancel </Button>
																	      
																	    		{
																	    		!isClicked ? (
																	    					<Button color="green" onClick={handleLogin} className="flex mt-8 flex-row capitalize hover:scale-105 items-center" variant="filled"> Sign In </Button>
																	    				) : (
																	    						<Button color="green" disabled className="flex gap-1 flex-row mt-8 flex-row capitalize items-center" variant="filled"> <ImSpinner2 className="w-4 h-4 animate-spin" />Sign In </Button>
																	    				)
																	    		}

																	    </div>
																	</div>	
																	</form>


										</CardBody>
								</Card>
								<div className="absolute">
								  <ToastContainer position="bottom-center" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
								</div>	
						</div>

			)
}

export default Landing;