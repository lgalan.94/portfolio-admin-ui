import { Layout, InvisibleButton, CustomButton, CustomDialogBox, CustomDrawer } from '../components';
import { useState, useEffect } from 'react';
import { Accordion, AccordionHeader, AccordionBody, Input, Textarea, Typography } from '@material-tailwind/react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { TiChevronLeft } from "react-icons/ti";
import { FaPlusCircle, FaMinusCircle } from "react-icons/fa";

/********certificates*********/
//https://drive.google.com/uc?export=view&id
let backend = 'https://drive.google.com/uc?export=view&id=15XGRw1S6uzL-A2Kgt_4aHMnzQRsqpBxH';
let bootcamp_completion = "https://drive.google.com/uc?export=view&id=1uFZAsnjvUT_Kp7owpxtvisG2ArWbE5Gv";
let ca_talk_1 = "https://drive.google.com/uc?export=view&id=1rV1OKxIrjYD6Ngf-x3-TqwrQ8VIQJQe1";
let ca_talk_2 = "https://drive.google.com/uc?export=view&id=1B-K9OuUysLgDUaeH73Dhhsu8xVX2ngUW";
/*****************************/

const Projects = () => {

		let i = 1;
		const [items, setItems] = useState([]);
		const [isLoading, setIsLoading] = useState(true);
		let [isCard2Open, setIsCard2Open] = useState(true);
		const handleCardOpen = () => setIsCard2Open(!isCard2Open);
		const [title, setTitle] = useState('');
		const [imgUrl, setImgUrl] = useState('');
		const [description, setDescription] = useState('');
		const [projectLink, setProjectLink] = useState('');
		const [isAddButtonClicked, setIsAddButtonClicked] = useState(false);
		const [technologiesUsed, setTechnologiesUsed] = useState([]);

		const handleAddTechnology = () => {
	    setTechnologiesUsed([...technologiesUsed, { name: '' }]);
	  };

	  const handleRemoveTechnology = (index) => {
	    const updatedTechnologies = [...technologiesUsed];
	    updatedTechnologies.splice(index, 1);
	    setTechnologiesUsed(updatedTechnologies);
	  };

	  const handleInputChange = (event, index) => {
	    const { value } = event.target;
	    const updatedTechnologies = [...technologiesUsed];
	    updatedTechnologies[index] = { name: value };
	    setTechnologiesUsed(updatedTechnologies);
	  };

		const handleClear = () => {
					setTitle('');
					setImgUrl('');
					setDescription('');
					setProjectLink('');
					setTechnologiesUsed([{ name: '' }])
					setIsAddButtonClicked(false);
		}

		const fetchProjects = () => {
					fetch(`${import.meta.env.VITE_API_URL}/projects/`, {
								method: 'GET',
								headers: {
									'Content-Type': 'application/json'
								}
					})
					.then(response => response.json())
					.then(data => {
								data.sort();
								if (data.length > 0) {
											setItems(data.map(item => {
														return (
																		<CustomCard key={item._id} itemProps={item} />
															)
											}))
											setIsLoading(false);
								} else {
										setItems(<div>No data!</div>)
										setIsLoading(false);
								}
					})
					.catch(error => console.error)
		}

		const refresh = () => {
					setIsLoading(true);
					fetchProjects();
					setTimeout(() => setIsLoading(false), 5000);
					handleClear();
		}

		useEffect(() => {
				fetchProjects();
				refresh();
		}, [])

		const projectData = {
		  title,
		  description,
		  imgUrl,
		  projectLink,
		  technologiesUsed,
		};

		const handleAdd = (e) => {
				e.preventDefault();
				setIsAddButtonClicked(true);
				
				fetch(`${import.meta.env.VITE_API_URL}/projects/add`, {
						method: 'POST',
						headers: {
								'Content-Type': 'application/json'
						},
						body: JSON.stringify(projectData),
				})
				.then(response => response.json())
				.then(data => {
								if (data === true) {
											toast.success('New data successfully added!');
											handleClear();
											fetchProjects();
								} else {
										toast.error('Error adding new data!');
										handleClear();
								}
				})
				.catch(error => console.error);
		}
 
		/******************************************/
			let CustomCard = (props) => {

				let view = "https://drive.google.com/uc?export=view&id=";
				const { _id, title, description, projectLink, imgUrl, technologiesUsed } = props.itemProps;
				let [open, setOpen] = useState(0);
				let [isDeleteButtonClicked, setIsDeleteButtonClicked] = useState(false);
				let [openDialog, setOpenDialog] = useState(false);
				let [isUpdateClicked, setIsUpdateClicked] = useState(false);

				const [newTitle, setNewTitle] = useState(title, (prevTitle) => prevTitle); // Access previous key
				const [newImgUrl, setNewImgUrl] = useState(imgUrl, (prevImgUrl) => prevImgUrl); // Access previous value

				const handleTitleChange = (e) => {
							setNewTitle((prevTitle) => e.target.value); 
				}

				const handleImgUrlChange = (e) => {
							setNewImgUrl((prevImgUrl) => e.target.value); 
				}

				const handleOpenDialog = () => setOpenDialog(!openDialog);
				const handleOpen = (value) => setOpen(open === value ? 0 : value);
			

				const handleDelete = (e) => {
						e.preventDefault();
						setIsDeleteButtonClicked(true);
						fetch(`${import.meta.env.VITE_API_URL}/projects/${_id}`, {
										method: 'DELETE'
						})
						.then(response => response.json())
						.then(data => {
									if (data === true) {
												toast.success('Successfully deleted!');
												fetchProjects();
									} else {
											toast.error('Error deleting data!')
									}
						})
						.catch(error => console.error)
				}


						return (
							<>
									
					    <div className="text-center flex flex-col gap-4 p-4 border border-1 border-dark/25 rounded-md group hover:scale-105 shadow-lg"> 
					    		<div>
					    				<Typography className="capitalize" variant="h6"> {title} </Typography>
					    		</div>
				    			<div>
				    					<img src={imgUrl} />
				    			</div>
					    			<div className="flex flex-row text-xs gap-2 justify-center flex-wrap font-ibm">
					    					{
					    							technologiesUsed.map(item => {
					    										return (
					    													<div key={item._id} className="px-2 py-1 uppercase border-l-2 shadow-md border-[#2ec946] bg-[#2ec946]/4">{item.name}</div>
					    										)
					    							})
					    					}
					    			</div>
					    			<div className="mt-4">
					    					<Typography className="text-green-400 text-sm">Description</Typography>
					    					<Typography className="text-sm"> {description} </Typography>
					    			</div>
					    			<div>
					    					<p className="text-green-400 text-sm">Project Link</p>
					    					<p className="text-xs italic underline lowercase">{projectLink}</p>
					    			</div>
					    			<InvisibleButton
					    					icon={<RiDeleteBin3Fill />}
					    					label="delete"
					    					tooltip="!bg-red-500"
					    					placement="bottom"
					    					btn="!text-red-500 absolute top-2 right-2"
					    					handleClick={handleOpenDialog}
					    			/>
					    </div>

 			    	<CustomDialogBox
 			    			title="Confirmation"
 			    			open={openDialog}
 			    			handleOpen={handleOpenDialog}
 			    	>
 			    				Delete {title}?
 			    			
 			    			<div className="mt-10 flex flex-row justify-center gap-1">
 			    					<CustomButton
 			    							label="cancel"
 			    							handleClick={handleOpenDialog}
 			    							tooltip="hidden"
 			    					/>
 			    					{
 			    							!isDeleteButtonClicked ? (
    													<CustomButton
    															label="delete"
    															color="red"
    															handleClick={handleDelete}
    															tooltip="hidden"
    													/>
 			    								):(
 			    									<CustomButton
 			    											label="deleting..."
 			    											color="red"
 			    											isDisabled={true}
 			    											tooltip="hidden"
 			    									/>
 			    							)
 			    					}
 			    			</div>

 			    </CustomDialogBox>
				  </>
						)
			}
		/********/
			return (
						<Layout
							refresh={refresh}
							pageTitle="Projects"
							loading={
									isLoading ? '' : 'hidden'
							}
							childrenClass={
									isLoading ? 'hidden' : ''
							}
							card2={
									isCard2Open ? '!min-w-[19rem]' : 'hidden'
							}
							component3={
										<CustomButton
												tooltip="hidden"
												variant="text"
												color=""
												icon={<TiChevronLeft className={`w-4 h-4 ${isCard2Open ? 'rotate-180 transition-transform delay-75' : ''}`} />}
												handleClick={handleCardOpen}
										/>
							}
							component4="Add Project Form"
							children2={
										<div className="flex flex-col gap-2">
										      <Input
										        label="Title"
										        className="capitalize"
										        color="teal"
										        value={title}
										        onChange={(e) => setTitle(e.target.value)}
										      />
										      <Textarea
										        label="Description"
										        color="teal"
										        value={description}
										        className="h-full min-h-[50px] scrollbar-thin"
										        onChange={(e) => setDescription(e.target.value)}
										      />
										      <div className="-mt-2">
										      		<Textarea
										      		  label="Project Link"
										      		  color="teal"
										      		  value={projectLink}
										      		  className="h-full min-h-[50px] scrollbar-thin"
										      		  onChange={(e) => setProjectLink(e.target.value)}
										      		/>
										      </div>
										      <div className="-mt-2">
										      		<Textarea
										      		  label="Img Url"
										      		  color="teal"
										      		  value={imgUrl}
										      		  className="h-full min-h-[50px] scrollbar-thin"
										      		  onChange={(e) => setImgUrl(e.target.value)}
										      		/>
										      </div>
										      <div className="flex px-1 py-2 rounded-md flex-col gap-1">
										        <label className="text-sm">Technologies Used</label>
										        {technologiesUsed.map((tech, index) => (
										          <div className="flex flex-row gap-1" key={index}>
										            <Input
										              label={`${index + 1}`}
										              className="uppercase"
										              color="teal"
										              value={tech.name}
										              onChange={(e) => handleInputChange(e, index)}
										            />
										            <button className="text-red-500" onClick={() => handleRemoveTechnology(index)}>
										            		<FaMinusCircle className="h-3 w-3" />
										            </button>
										          </div>
										        ))}
										        <div className="flex flex-row justify-center">
										          <CustomButton
										            icon={<FaPlusCircle className="h-5 w-5 " />}
										            handleClick={handleAddTechnology}
										            tooltip="hidden"
										            variant="text"
										            color="green"
										          />
										        </div>
										      </div>
										      <div className="flex flex-row justify-center gap-1 mt-6">
										        {!isAddButtonClicked ? (
										          <CustomButton
										            label="Save"
										            color="green"
										            handleClick={handleAdd}
										            tooltip="hidden"
										          />
										        ) : (
										          <CustomButton
										            label="Saving..."
										            color="green"
										            isDisabled={true}
										            icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
										            tooltip="hidden"
										          />
										        )}
										        <CustomButton
										          label="Clear"
										          handleClick={handleClear}
										          tooltip="hidden"
										        />
										      </div>
										    </div>
							}
							copyRight={
										isCard2Open ? 'hidden' : ''
							}
							itemNumber={
											items.length > 0 ? `${items.length} items` : 'No Data'
							}
						>
								<div className="grid grid-cols-2 gap-4">
										{items}
								</div>
						</Layout>
			)
}

export default Projects