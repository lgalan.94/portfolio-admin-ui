import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import DataContext from '../DataContext.js';

export default function Logout() {
  const { unsetData, setData } = useContext(DataContext);

  useEffect(() => {
    unsetData();
    setData({
      id: null,
      email: null,
      isAdmin: null,
    });
  }, []);

  return <Navigate to="/" />;
}