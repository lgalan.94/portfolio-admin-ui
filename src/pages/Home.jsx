import { Layout, CustomButton, CustomDialogBox, InvisibleButton, CustomDrawer, CustomMenu } from '../components';
import { useState, useEffect } from 'react';
import { MdOutlineAdd } from "react-icons/md";
import { Textarea, Accordion, AccordionHeader, AccordionBody, Input, Typography, Menu, Tooltip, MenuList, MenuHandler, MenuItem, Button } from '@material-tailwind/react';
import { TiChevronLeft } from "react-icons/ti";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { BiSort } from "react-icons/bi";


const Home = () => {
	const [isLoading, setIsLoading] = useState(true);
	const [settings, setSettings] = useState([]);
	const [isClicked, setIsClicked] = useState(true);
	const [isAddClicked, setIsAddClicked] = useState(false);
	const [key, setKey] = useState('');
	const [value, setValue] = useState('');
	
	const handleCollapse = () => setIsClicked(!isClicked);
	const handleClear = () => {
				setKey('');
				setValue('');
	}

	const fetchSettings = () => {
				fetch(`${import.meta.env.VITE_API_URL}/settings/all-settings`, {
						method: 'GET',
						headers: {
								'Content-type': 'application/json'
						}
				})
				.then(response => response.json())
				.then(data => {
								data.sort((a, b) => b.createdOn.localeCompare(a.createdOn));
								if (data.length > 0) {
										setSettings(data.map(item => (
															<SettingsCard key={item._id} SettingsProps={item} />
											)))
										setTimeout(() => setIsLoading(false), 1000)
								} else {
										setSettings(<div>No Data</div>)
								}
								setIsLoading(false)
				})
				.catch(error => console.error)
	}

const refresh = () => {
			fetchSettings();
			setIsLoading(true);
			setTimeout(() => setIsLoading(false), 1000)
}

const handleAdd = (event) => {
  event.preventDefault();
  setIsAddClicked(true);
  fetch(`${import.meta.env.VITE_API_URL}/settings/add-key-value`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      key: key,
      value: value
    })
  })
    .then(result => result.json())
    .then(data => {
      if (data === true) {
        toast.success("New Data Successfully Added!");
        fetchSettings();
        setKey('');
        setValue('');
        setIsAddClicked(false);
      } else if (!data.isAdmin) {
      		toast.info("Current user role is view-mode only!")
      		setTimeout(() => toast.info("Login as admin to add new data!"), 1500);
      		setKey("");
      		setValue("");
      		setIsAddClicked(false);
      } else {
        toast.error('Error adding new data!');
        setTimeout(() => 1000);
        setKey('');
        setValue('');
        setIsAddClicked(false);
      }
    });
};

useEffect(() => {
			fetchSettings();
			refresh();
}, [])

/**************************************/
const SettingsCard = (props) => {

	let { _id, key, value, createdOn, handlePop } = props.SettingsProps;
	let [date, setDate] = useState(createdOn);
	const [openDialog, setOpenDialog] = useState(false);
	const [title, setTitle] = useState('');
	const [open, setOpen] = useState(0); 
	const [isClicked, setIsClicked] = useState(false);
	const [isUpdateClicked, setIsUpdateClicked] = useState(false);
	const [openDrawer, setOpenDrawer] = useState(false);

	const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
	const handleCloseDrawer = () => setOpenDrawer(false);
	const handleOpenDialog = () => setOpenDialog(!openDialog);	
	const handleOpen = (value) => setOpen(open === value ? 0 : value);
	
	const [newKey, setKey] = useState(key, (prevKey) => prevKey); // Access previous key
	const [newValue, setValue] = useState(value, (prevValue) => prevValue); // Access previous value

	const handleKeyChange = (e) => {
				setKey((prevKey) => e.target.value); 
	}

	const handleValueChange = (e) => {
				setValue((prevValue) => e.target.value); 
	}


	  const handleDelete = (e) => {
	  		e.preventDefault();
	  		setIsClicked(true);
	  		fetch(`${import.meta.env.VITE_API_URL}/settings/${_id}`, {
	  					method: 'DELETE',
	  					headers: {
	  							'Authorization': `Bearer ${localStorage.getItem('token')}`
	  					}
	  		})
	  		.then(response => response.json())
	  		.then(data => {
	  					if (data === true) {
	  							toast.success(`${key} Successfully Deleted!`);
	  							fetchSettings()
	  							refresh()
	  							setOpenDialog(false)
	  							setIsClicked(false)
	  					} else {
	  							toast.error('Delete Error! ');
	  							setTimeout(() => 1000);
	  							setOpenDialog(false)
	  							setIsClicked(false)
	  					}
	  		})
	  		.catch(error => console.error)
	  };

	 const handleUpdate = (event) => {
	   event.preventDefault();
	   setIsUpdateClicked(true);
	   fetch(`${import.meta.env.VITE_API_URL}/settings/${_id}`, {
	     method: 'PATCH',
	     headers: {
	       'Content-Type': 'application/json',
	       'Authorization': `Bearer ${localStorage.getItem('token')}`
	     },
	     body: JSON.stringify({
	       newKey,
	       newValue
	     })
	   })
	     .then(response => response.json())
	     .then(data => {
	       if (data === true) {
	         toast.success("Update Successful!");
	         fetchSettings();
	         refresh();
	         setOpenDrawer(false);
	         setIsUpdateClicked(false);
	       } else {
	         toast.error('Error updating data!');
	         setTimeout(() => 1000);
	         setIsUpdateClicked(false);
	         setOpenDrawer(false);
	       }
	     });
	 };

			return (
						<>
							<CustomDrawer
									open={openDrawer}
									openDrawer={handleOpenDrawer}
									closeDrawer={handleCloseDrawer}
							>
									<form className="flex self-center flex-row gap-4 p-10">
									  <div className="w-2/5">
									  		<Input
									  				disabled
									  				label="KEY" 
									  				value={newKey}
									  				onChange={handleKeyChange}
									  				color="teal"
									  		/>
									  </div>
									  <div className="w-3/5">
									  		<Textarea
									  			label="VALUE"
									  			value={newValue}
									  			onChange={handleValueChange}
									  			color="teal"
									  			className="min-h-[3rem] scrollbar-thin"
									  			resize={true}
									  		/>
									  </div>
									</form>
									<div className="flex flex-row mt-4 gap-2 justify-center">
											<CustomButton 
													label="Cancel"
													handleClick={handleCloseDrawer}
											/>
											{
													!isUpdateClicked ? (
																<CustomButton 
																		label="Update"
																		handleClick={handleUpdate}
																		color="green"
																		tooltip="hidden"
																/>
														) : (
																<CustomButton 
																		label="Updating..."
																		icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																		color="green"
																		isDisabled={true}
																		tooltip="hidden"
																/>
														)
											}
									</div>
							</CustomDrawer>
			    <Accordion open={open === 1} className="rounded-lg border border-blue-gray-300 w-full w-full max-w-[14rem] group px-4">
         <AccordionHeader
           onClick={() => handleOpen(1)}
           className={`border-b-0 transition-colors font-ibm  ${
             open === 1 ? "text-green-500 hover:!text-green-700" : ""
           }`}
         >
           <div className="flex flex-col">
           		{key} 
           		<span className="text-xs">
  								{new Date(createdOn).toLocaleString('en-US', { dateStyle: 'short', timeStyle: 'short' })}
				  		</span>
				   </div>
           <InvisibleButton
           		icon={<MdRebaseEdit />}
           		label="edit"
           		tooltip="!bg-green-500"
           		placement="top"
           		btn="!text-green-500 absolute right-12"
           		handleClick={handleOpenDrawer}
           />
           <InvisibleButton
           		icon={<RiDeleteBin3Fill />}
           		label="delete"
           		tooltip="!bg-red-500"
           		placement="top"
           		btn="!text-red-500 absolute right-2"
           		handleClick={handleOpenDialog}
           />
         </AccordionHeader>
         <AccordionBody className="pt-0 font-ibm text-sm font-normal">
       					{value}
         </AccordionBody>
       </Accordion>
			    	

			    	<CustomDialogBox
			    			title="Confirmation"
			    			open={openDialog}
			    			handleOpen={handleOpenDialog}
			    	>
			    				Delete {key}?
			    			
			    			<div className="mt-10 flex flex-row justify-center gap-1">
			    					<CustomButton
			    							label="cancel"
			    							handleClick={handleOpenDialog}
			    							tooltip="hidden"
			    					/>
			    					{
			    							!isClicked ? (
   													<CustomButton
   															label="delete"
   															color="red"
   															handleClick={handleDelete}
   															tooltip="hidden"
   													/>
			    								):(
			    									<CustomButton
			    											label="deleting..."
			    											color="red"
			    											isDisabled={true}
			    											tooltip="hidden"
			    									/>
			    							)
			    					}
			    			</div>

			    </CustomDialogBox>
						</>
			)
}
/**************************************/

			return (
					<>
							<Layout
									loading={
											isLoading ? '' : 'hidden'
									}
									childrenClass={
											isLoading ? 'hidden' : ''
									}
									refresh={refresh}
									pageTitle="Home"
									component3={
												<CustomButton
														tooltip="hidden"
														variant="text"
														color=""
														icon={<TiChevronLeft className={`w-4 h-4 ${isClicked ? 'rotate-180 transition-transform delay-75' : ''}`} />}
														handleClick={handleCollapse}
												/>
									}
									card2={
												isClicked ? '' : 'hidden'
									}
									component4="Add New"
									children2={
												<div className="flex flex-col gap-3">
														<Input
																label="KEY"
																color="teal"
																value={key}
																onChange={(e) => setKey(e.target.value)}
														/>
														<Textarea
																label="VALUE"
																color="teal"
																value={value}
																onChange={(e) => setValue(e.target.value)}
														/>
															<div className="flex flex-row justify-center gap-1 mt-6">
																	{
																				!isAddClicked ? (
																							<CustomButton
																									label="Save"
																									color="green"
																									handleClick={handleAdd}
																									tooltip="hidden"
																							/>
																					) : (
																							<CustomButton
																									label="Saving..."
																									color="green"
																									isDisabled={true}
																									icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																									tooltip="hidden"
																							/>
																					)
																	}
																	<CustomButton
																			label="Clear"
																			handleClick={handleClear}
																			tooltip="hidden"
																	/>
															</div>
												</div>
									}
									copyRight={
												isClicked ? 'hidden' : ''
									}
									itemNumber={
													settings.length > 0 ? `${settings.length} items` : 'No Data'
									}
							>
									<div className="flex flex-row justify-center flex-wrap gap-3">
											{
													settings
											}
									</div>
							</Layout>	
					</>
			)
}

export default Home