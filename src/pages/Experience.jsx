import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Layout, CustomButton, InvisibleButton, CustomDialogBox, CustomDrawer } from '../components';
import { TiChevronLeft } from "react-icons/ti";
import { RiRefreshFill } from "react-icons/ri";
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { FaRegCheckCircle } from "react-icons/fa";
import { useState, useEffect } from 'react';
import { Accordion, AccordionHeader, AccordionBody, Typography, Input, Textarea, Alert, Drawer } from '@material-tailwind/react';


const Experience = () => {

	let token = localStorage.getItem('token');
	const [items, setItems] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [isCollapsed, setIsCollapsed] = useState(false);
	const handleCollapse = () => setIsCollapsed(!isCollapsed);
	const [isSave, setIsSave] = useState(false);
	const [position, setPosition] = useState('');
		const [company, setCompany] = useState('');
		const [companyLink, setCompanyLink] = useState('');
		const [timeRange, setTimeRange] = useState('');
		const [address, setAddress] = useState('');
		const [work, setWork] = useState('');
		const [successAlert, setSuccessAlert] = useState(false);
		let [updateAlert, setUpdateAlert] = useState(false);

	/**************Fetch Data**************/
	const fetchEducation = () => {
		 		fetch(`${import.meta.env.VITE_API_URL}/experience`, {
		 				method: 'GET',
		 				headers: {
		 						'Content-type': 'application/json'
		 				}
		 		})
		 		.then(response => response.json())
		 		.then(data => {
		 						if (data.length > 0) {
		 									setItems(data.map(item => {
		 											return (
		 														<CustomCard key={item._id} itemProps={item} />
		 											)
		 									}))
		 									setIsLoading(false);
		 						} else {
		 									setItems(<div>No data in the database!</div>)
		 									setIsLoading(false);
		 						}
		 		})
		 		.catch(error => console.error) 
		} 

	const refresh = () => {
			setIsLoading(true);
			fetchEducation();
			setTimeout(() => setIsLoading(false), 4000)
	}

		useEffect(() => {
					fetchEducation();
					refresh();
		}, [])

		const handleClear = () => {
				setPosition('');
				setTimeRange('');
				setCompany('');
				setCompanyLink('');
				setAddress('');
				setWork('');
				setIsSave(false);
		}

		const handleAdd = (event) => {
				  event.preventDefault();
				  setIsSave(true);
				  fetch(`${import.meta.env.VITE_API_URL}/experience/add`, {
				    method: 'POST',
				    headers: {
				      'Content-Type': 'application/json',
				      'Authorization': `Bearer ${token}`
				    },
				    body: JSON.stringify({
				      position: position,
		     			company: company,
		     			companyLink: companyLink,
		     			timeRange: timeRange,
		     			address: address,
		     			work: work
				    })
				  })
				    .then(result => result.json())
				    .then(data => {
				      	if (data === true) {
				      	  fetchEducation();
				      	  setSuccessAlert(true);
				      	 	setIsSave(false);
				      	 	handleClear();
				      	 	setTimeout(() => setSuccessAlert(false), 4500);
				      	} else if (!data.isAdmin) {
				      			toast.info("Current user role is view-mode only!")
				      			setTimeout(() => toast.info("Login as admin to add new data!"), 1500);
				      			handleClear();
				      	} else {
				      	  toast.error('Error adding new data!');
				      	  setTimeout(() => 1000);
				      	  setIsSave(false);
				      	  handleClear()
				      	}
				    })
				    .catch(error => console.error);
				};

	/**************************************/
				
	/***********Card Props*************/
		const CustomCard = (props) => {
			const { _id, position, company, companyLink, timeRange, address, work, createdOn } = props.itemProps;
			const [isAccordionOpen, setIsAccordionOpen] = useState(0);
			const handleAccordionOpen = (value) => setIsAccordionOpen(isAccordionOpen === value ? 0 : value);
			const [openDialog, setIsOpenDialog] = useState(false);
			const handleOpenDialog = () => setIsOpenDialog(!openDialog);
			const [isClicked, setIsClicked] = useState(false);
			let [updatedPosition, setUpdatedPosition] = useState(position);
			let [updatedCompany, setUpdatedCompany] = useState(company);
			let [updatedCompanyLink, setUpdatedCompanyLink] = useState(companyLink);
			let [updatedTimeRange, setUpdatedTimeRange] = useState(timeRange);
			let [updatedAddress, setUpdatedAddress] = useState(address);
			let [updatedWork, setUpdatedWork] = useState(work);
			let [isUpdateClicked, setIsUpdateClicked] = useState(false);

			const [openDrawer, setOpenDrawer] = useState(false);

			const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
			const handleCloseDrawer = () => setOpenDrawer(false);
			

			const handleDelete = (e) => {
						e.preventDefault();
						setIsClicked(true);
						fetch(`${import.meta.env.VITE_API_URL}/experience/delete/${_id}`, {
									method: 'DELETE',
									headers: {
											'Authorization': `Bearer ${token}`
									}
						})
						.then(response => response.json())
						.then(data => {
										if (data === true) {
													toast.success('Successfully Deleted!');
													fetchEducation();
													setIsOpenDialog(false);
													setIsClicked(false);
										} else {
													toast.error('Unable to remove data!');
													setIsOpenDialog(false);
													setIsClicked(false);
										}
						})
						.catch(error => console.error)
			}

			const handleUpdate = (e) => {
						e.preventDefault();
						setIsUpdateClicked(true);
						fetch(`${import.meta.env.VITE_API_URL}/experience/update/${_id}`, {
									method: "PATCH",
									headers: {
											'Content-type': 'application/json',
											'Authorization': `Bearer ${token}`
									},
									body: JSON.stringify({
											updatedPosition,
											updatedCompany,
											updatedCompanyLink,
											updatedTimeRange,
											updatedAddress,
											updatedWork
									})
						})
						.then(response => response.json())
						.then(data => {
									if (data === true) {
												setUpdateAlert(true);
												fetchEducation();
												setIsUpdateClicked(false);
												setOpenDrawer(false);
												setTimeout(() => setUpdateAlert(false), 4500)
									} else {
												toast.error('Unable to update data!');
												setOpenDrawer(false);
									}
						})
						.catch(error => console.error)
			}

				return (
						<>
							<CustomDrawer
										open={openDrawer}
										openDrawer={handleOpenDrawer}
										closeDrawer={handleCloseDrawer}
								>
										<div className="flex flex-col font-ibm gap-3">
												<div className="flex flex-row gap-3 px-10">
														<Input
																label="POSITION"
																color="teal"
																value={updatedPosition}
																onChange={(e) => setUpdatedPosition(e.target.value)}
														/>
														<Input
																label="YEAR RANGE"
																color="teal"
																value={updatedTimeRange}
																onChange={(e) => setUpdatedTimeRange(e.target.value)}
														/>
														<Input
																label="COMPANY"
																color="teal"
																value={updatedCompany}
																onChange={(e) => setUpdatedCompany(e.target.value)}
														/>
												</div>
												
												<div className="flex flex-row gap-3 px-10">
														<div className="w-full">	
															<Textarea
																	label="COMPANY LINK"
																	className="h-full min-h-[50px] scrollbar-thin"
																	color="teal"
																	value={updatedCompanyLink}
																	onChange={(e) => setUpdatedCompanyLink(e.target.value)}
															/>
														</div>
														
														<div className="w-full">	
															<Textarea
																	label="ADDRESS"
																	className="h-full min-h-[50px] scrollbar-thin"
																	color="teal"
																	value={updatedAddress}
																	onChange={(e) => setUpdatedAddress(e.target.value)}
															/>
														</div>
												</div>
												
												<div className="w-full px-10">	
													<Textarea
															label="WORK"
															className="h-full min-h-[50px] scrollbar-thin"
															color="teal"
															value={updatedWork}
															onChange={(e) => setUpdatedWork(e.target.value)}
													/>
												</div>
													<div className="flex flex-row justify-center gap-1 mt-4">
															{
																		!isUpdateClicked ? (
																					<CustomButton
																							label="Update"
																							color="green"
																							tooltip="hidden"
																							handleClick={handleUpdate}
																					/>
																			) : (
																					<CustomButton
																							label="Updating..."
																							color="green"
																							isDisabled={true}
																							icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																							tooltip="hidden"
																					/>
																			)
															}
															<CustomButton
																	label="Clear"
																	tooltip="hidden"
															/>
													</div>
										</div>
								</CustomDrawer>
			    <Accordion open={isAccordionOpen === 1} className="rounded-lg border border-blue-gray-300 w-full w-full max-w-[20rem] group px-4">
	        <AccordionHeader
	          onClick={() => handleAccordionOpen(1)}
	          className={`border-b-0 transition-colors font-ibm  ${
	            isAccordionOpen === 1 ? "text-green-500 hover:!text-green-700" : ""
	          }`}
	        >
	          {position}
	          <InvisibleButton
	          		icon={<MdRebaseEdit />}
	          		label="edit"
	          		tooltip="!bg-green-500"
	          		placement="top"
	          		btn="!text-green-500 absolute right-12"
	          		handleClick={handleOpenDrawer}
	          />
	          <InvisibleButton
	          		icon={<RiDeleteBin3Fill />}
	          		label="delete"
	          		tooltip="!bg-red-500"
	          		placement="top"
	          		btn="!text-red-500 absolute right-2"
	          		handleClick={handleOpenDialog}
	          />
	        </AccordionHeader>
	        <AccordionBody className="pt-0 font-ibm text-sm font-normal">
								    <Typography className="text-sm">
								      {company}
								    </Typography>
								    <Typography className="text-sm">
								      {companyLink}
								    </Typography>
								    <Typography className="text-sm">
								      {timeRange}
								    </Typography>
								    <Typography className="text-sm">
								      {address}
								    </Typography>
								    <Typography className="text-sm">
								      {work}
								    </Typography>
	        </AccordionBody>
	      </Accordion>
		    	<CustomDialogBox
		    			title="Confirmation"
		    			open={openDialog}
		    			handleOpen={handleOpenDialog}
		    	>
		    				Delete {position}?
		    			
		    			<div className="mt-10 flex flex-row justify-center gap-1">
		    					<CustomButton
		    							label="cancel"
		    							handleClick={handleOpenDialog}
		    							tooltip="hidden"
		    					/>
		    					{
		    							!isClicked ? (
	 													<CustomButton
	 															label="delete"
	 															color="red"
	 															handleClick={handleDelete}
	 															tooltip="!hidden"
	 													/>
		    								):(
		    									<CustomButton
		    											label="deleting..."
		    											color="red"
		    											isDisabled={true}
		    											tooltip="!hidden"
		    									/>
		    							)
		    					}
		    			</div>
		    </CustomDialogBox>
		    </>
					)
			}
	
	/**********************************/
			return (
						<Layout
								loading={
											isLoading ? '' : 'hidden'
								}
								childrenClass={
											isLoading ? 'hidden' : ''
								}
								refresh={refresh}
								pageTitle="work experience"
								component3={
											<CustomButton
													tooltip="hidden"
													variant="text"
													color=""
													icon={<TiChevronLeft className={`w-4 h-4 ${!isCollapsed ? 'rotate-180 transition-transform delay-75' : ''}`} />}
													handleClick={handleCollapse}
											/>
								}
								component4="Add New"
								card2={
											isCollapsed ? 'hidden' : '!min-w-[18rem]'
								}
								copyRight={
											!isCollapsed ? 'hidden' : ''
								}
								itemNumber={
												items.length > 0 ? `${items.length} items` : 'No Data'
								}
								children2={
											<div className="flex flex-col font-ibm gap-3">
													<Input
															label="POSITION"
															color="teal"
															value={position}
															onChange={(e) => setPosition(e.target.value)}
													/>
													<Input
															label="YEAR RANGE"
															color="teal"
															value={timeRange}
															onChange={(e) => setTimeRange(e.target.value)}
													/>

													<div className="">	
														<Textarea
																label="COMPANY"
																className="h-full min-h-[60px] scrollbar-thin"
																color="teal"
																value={company}
																onChange={(e) => setCompany(e.target.value)}
														/>
													</div>
													
													<div className="-mt-2">	
														<Textarea
																label="COMPANY LINK"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={companyLink}
																onChange={(e) => setCompanyLink(e.target.value)}
														/>
													</div>
													
													<div className="-mt-2">	
														<Textarea
																label="ADDRESS"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={address}
																onChange={(e) => setAddress(e.target.value)}
														/>
													</div>
													<div className="-mt-2">	
														<Textarea
																label="WORK"
																className="h-full min-h-[50px] scrollbar-thin"
																color="teal"
																value={work}
																onChange={(e) => setWork(e.target.value)}
														/>
													</div>
														<div className="flex flex-row justify-center gap-1 mt-6">
																{
																			!isSave ? (
																						<CustomButton
																								label="Save"
																								color="green"
																								tooltip="hidden"
																								handleClick={handleAdd}
																						/>
																				) : (
																						<CustomButton
																								label="Saving..."
																								color="green"
																								isDisabled={true}
																								icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																								tooltip="hidden"
																						/>
																				)
																}
																<CustomButton
																		label="Clear"
																		tooltip="hidden"
																		handleClick={handleClear}
																/>
														</div>
											</div>
								}
						>	
								<div className="w-full"> 
										<Alert
														size="sm"
								      icon={<FaRegCheckCircle className="w-4 h-4" />}
								      animate={{
								      	mount: { y: 0 },
								      	unmount : {y: 1},
								      }}
								      open={successAlert}
								      className="rounded-md mb-2 p-2 border-l-4 border-[#2ec946] bg-[#2ec946]/10 text-sm text-[#2ec946]"
								    >
								      New data added!
								    </Alert>
								</div>
								<div className="w-full"> 
										<Alert
														size="sm"
								      icon={<FaRegCheckCircle className="w-4 h-4" />}
								      animate={{
								      	mount: { y: 0 },
								      	unmount : {y: 1},
								      }}
								      open={updateAlert}
								      className="rounded-md mb-2 p-2 border-l-4 border-[#2ec946] bg-[#2ec946]/10 text-sm text-[#2ec946]"
								    >
								      Successfully updated!
								    </Alert>
								</div>
								<div className="flex flex-row justify-center flex-wrap gap-3">
											{items}
								</div>
						</Layout>
			)
}

export default Experience