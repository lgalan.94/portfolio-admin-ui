import { Layout, CustomButton } from '../components';
import { Input, Card, CardBody, Typography }  from '@material-tailwind/react';
import { useState } from 'react';
import avatar from '../assets/avatar.jpg'

const ConvertImage = () => {

	const [postImage, setPostImage] = useState({ myFile : ""})
	const [url, setUrl] = useState('');
	const [imageSize, setImageSize] = useState(0);
	const [isCopied, setIsCopied] = useState(false);

	const handleFileUpload = async (e) => {
	  const file = e.target.files[0];
	  const base64 = await convertToBase64(file);
	  setUrl(base64);
	  setPostImage({ ...postImage, myFile : base64 })
	  const reader = new FileReader();

	  reader.onload = () => {
	    const image = new Image();
	    image.onload = () => {
	      const imageSizeInBytes = image.naturalWidth * image.naturalHeight * 2; // Assuming RGBA
	      const imageSizeInKB = Math.round(imageSizeInBytes / 1024);

	      // Update state or data to reflect the image size
	      setImageSize(imageSizeInKB); // Assuming you're using state or a similar mechanism
	    };
	    image.src = reader.result;
	  };

	  reader.readAsDataURL(file);
	}


	function convertToBase64(file){
	  return new Promise((resolve, reject) => {
	    const fileReader = new FileReader();
	    fileReader.readAsDataURL(file);
	    fileReader.onload = () => {
	      resolve(fileReader.result)
	    };
	    fileReader.onerror = (error) => {
	      reject(error)
	    }
	  })
	}

	 const handleCopy = () => {
	  navigator.clipboard.writeText(url);
	  setIsCopied(true);
	  setTimeout(() => {
	    setIsCopied(false);
	  }, 3000);
	};

	const refresh = () => {

				setPostImage({ myFile : ""});
				setUrl(0);
				setImageSize(0);

	}

	const output = (
					<Card color="transparent" className="shadow-none border border-blue-gray-300 mt-24">
							<CardBody className="flex flex-col items-center justify-center text-sm">
									<div className="text-cyan-700 font-semibold">String Length:</div>
									<div>{url.length}</div>
									<div className="mt-4 text-cyan-700 font-semibold">Size in KB:</div>
									<div>{imageSize}</div>
								{/*<div className="">
								  <span className="italic"> </span> 
								  <span className="italic mt-4"> </span> {imageSize}
								</div>*/}
								<CustomButton
										label={isCopied ? "Copied" : "Copy"}
										btn="mt-6 mx-auto"
										variant="text"
										tooltip="hidden"
										handleClick={handleCopy}
								/>
							</CardBody>
					</Card>
		)

			return (
						<Layout
								refresh={refresh}
								loading="hidden"
								childrenClass=""
								pageTitle="Convert Image To String (Base64)"
								card2="min-w-[36vw]"
								children2={output}
						>
 						  <div className="flex flex-col gap-3 justify-center items-center"> 
		  					  <label htmlFor="file-upload" className='custom-file-upload'>
		  					    <img
		  					    		id="myImage"
		  	          className="w-[25rem] max-h-[20rem] rounded-md mb-2 mx-auto object-cover object-center"
		  	          src={postImage.myFile || avatar}
		  	          alt="nature image"
		  	        />
		  					  </label>
		  					  <div className="w-80">
		  					  		<Input 
		  					  		  type="file"
		  					  		  label="Image"
		  					  		  name="myFile"
		  					  		  id='file-upload'
		  					  		  accept='.jpeg, .png, .jpg'
		  					  		  onChange={(e) => handleFileUpload(e)}
		  					  		 />
		  					  </div>
 						  </div>
						</Layout>
			)
}

export default ConvertImage