import { Layout, CustomButton, InvisibleButton, CustomDialogBox, CustomDrawer } from '../components';
import { useState, useEffect } from 'react';
import { TiChevronLeft } from "react-icons/ti";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { ImSpinner2 } from "react-icons/im";
import { Input, Alert } from '@material-tailwind/react';
import { BiSolidError } from "react-icons/bi";
import { Typography } from '@material-tailwind/react';

const Messages = () => {
	let token = localStorage.getItem('token');
	const [messages, setMessages] = useState([]);
	let [isLoading, setIsLoading] = useState(true);
	let [isClicked, setIsClicked] = useState(false);
	let handleCollapse = () => setIsClicked(!isClicked);
	let [isSaveButtonClicked, setIsSaveButtonClicked] = useState(false);
	let [name, setName] = useState('');
	let [openAlert, setOpenAlert] = useState(false);
	let [inAlertName, setInAlertName] = useState('');

	let fetchMessages = () => {
				fetch(`${import.meta.env.VITE_API_URL}/messages`, {
						method: 'GET',
						headers: {
								'Content-Type': 'application/json'
						}
				})
				.then(response => response.json())
				.then(data => {
							data.sort((a, b) => b.createdOn.localeCompare(a.createdOn));
							if (data.length > 0) {
										setMessages(data.map(item => (
														<Message key={item._id} messageProps={item} />
											)))
										setIsLoading(false);
							} else {
										setIsLoading(false);
							}
				})
				.catch(error => console.error)
	}

	let refresh = () => {
				setIsLoading(true);
				fetchMessages();
				setTimeout(() => setIsLoading(false), 5000)
	}

	useEffect(() => {
			fetchMessages();
			refresh();
	}, [])


	/************Props************/
	const Message = (props) => {

		const { _id, name, email, message, createdOn } = props.messageProps;
		let [isButtonDeleteClicked, setIsButtonDeleteClicked] = useState(false);
		let [openDialog, setIsOpenDialog] = useState(false);
		const [openDrawer, setOpenDrawer] = useState(false);
		let handleOpenDialog = () => setIsOpenDialog(!openDialog);
		const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
		const handleCloseDrawer = () => setOpenDrawer(false);

		const handleDelete = () => {
					setIsButtonDeleteClicked(true);
					fetch(`${import.meta.env.VITE_API_URL}/messages/delete/${_id}`, {
								method: 'DELETE',
								headers: {
											'Authorization': `Bearer ${token}`
								}
					})
					.then(response => response.json())
					.then(data => {
								if (data === true) {
											toast.success('Successfully Deleted!');
											setOpenDrawer(false);
											fetchMessages();
											refresh();
											setIsOpenDialog(false);
											setIsButtonDeleteClicked(false);
								} else {
											toast.error('Unable to remove data!');
											setIsOpenDialog(false);
											setIsButtonDeleteClicked(false);
								}
					})
					.catch(error => console.error)
		}

			return (
					<>
							<CustomDrawer
									title={`From: ${name}`}
									open={openDrawer}
									openDrawer={handleOpenDrawer}
									closeDrawer={handleCloseDrawer}
							>
									 
											<div className="h-full max-h-[80vh] lg:max-h-[75vh] transform-none overflow-y-auto">
												<div className="flex flex-row justify-center gap-10">
												  <div className="mb-2">
												  	<Typography
												  	  variant="small"
												  	  className="font-normal"
												  	>
												  	  Email 
												  	</Typography>
												  	<Typography
												  	  variant="small"
												  	  className="font-normal"
												  	>
												  	 <span className="lowercase text-blue-300 font-semibold">{email}</span>
												  	</Typography>
												  </div>

												  <div className="">
												  	<Typography
												  	  variant="small"
												  	  className="font-normal"
												  	>
												  	  Date 
												  	</Typography>
												  	<Typography
												  	  variant="small"
												  	  className="font-normal"
												  	>
												  	 <span className="text-blue-300 font-semibold">
												  	 		{new Date(createdOn).toLocaleString('en-US', { dateStyle: 'full', timeStyle: 'medium' })}
												  	 </span>
												  	</Typography>
												  </div>

												</div>
												<div className="px-6">
												  	<Typography
												  	  variant="small"
												  	  className="font-normal uppercase"
												  	>
												  	  message
												  	</Typography>
												  	<Typography
												  	  variant="small"
												  	  className="tracking-wide focus:scale-[1.02] active:scale-100 mt-1 p-3 font-normal border border-gray-900/30 rounded h-full min-h-[5rem]"
												  	>
												  	  {message}
												  	</Typography>
												</div>
											</div>

									<div className="flex flex-row mt-6 gap-2 justify-center">
											<CustomButton 
													label="Close"
													handleClick={handleCloseDrawer}
											/>
											{
													!isButtonDeleteClicked ? (
																<CustomButton
																		color="red" 
																		label="Delete"
																		handleClick={handleOpenDialog}
																		tooltip="hidden"
																/>
														) : (
																<CustomButton
																		color="red" 
																		icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																		label="Deleting..."
																		isDisabled={true}
																		tooltip="hidden"
																/>
														)
											}
									</div>
							</CustomDrawer>
							<div className="text-center px-1 py-3 flex flex-row gap-2 uppercase border border-1 border-dark/25 rounded-md group hover:scale-105 shadow-lg"> 

										<InvisibleButton
												icon={<GiBoltEye className="w-3 h-3" />}
												label="view"
												placement="bottom"
												handleClick={handleOpenDrawer}
										/>

										<span>{name}</span>
										
										<InvisibleButton
												icon={<RiDeleteBin3Fill className="w-3 h-3" />}
												label="delete"
												tooltip="!bg-red-500"
												placement="bottom"
												btn="!text-red-500"
												handleClick={handleOpenDialog}
										/>
							</div>
		    	<CustomDialogBox
		    			title="Confirmation"
		    			open={openDialog}
		    			handleOpen={handleOpenDialog}
		    	>
		    				Delete message from {name}?
		    			
		    			<div className="mt-10 flex flex-row justify-center gap-1">
		    					<CustomButton
		    							label="cancel"
		    							handleClick={handleOpenDialog}
		    							tooltip="hidden"
		    					/>
		    					{
		    							!isButtonDeleteClicked ? (
		    											<CustomButton
		    													label="delete"
		    													color="red"
		    													handleClick={handleDelete}
		    													tooltip="hidden"
		    											/>
		    								) : (
		    										<CustomButton
		    												label="deleting..."
		    												color="red"
		    												isDisabled={true}
		    												tooltip="hidden"
		    										/>
		    								)
		    					}
		    			</div>
		    </CustomDialogBox>
					</>
			)
	}
	/*****************************/
			return (
						<Layout
								pageTitle="Messages"
								loading={
												isLoading ? '' : 'hidden'
								}
								childrenClass={
											isLoading ? 'hidden' : ''
								}
								card2="hidden"
								component4="Add New"
								copyRight={
											isClicked ? 'hidden' : ''
								}
								itemNumber={
												messages.length > 0 ? `${messages.length} messages` : 'No messages'
								}
								refresh={refresh}
						>

								<div className="w-full"> 
										<Alert
														icon={<BiSolidError className="w-5 h-5" />}
														size="sm"
								      animate={{
								      	mount: { y: 0 },
								      	unmount : {y: 1},
								      }}
								      open={openAlert}
								      className="rounded-md mb-2 p-2 border-l-4 border-red-500 bg-red-500/20 text-sm text-red-500"
								    >
								      	{inAlertName.toUpperCase()} already exists!
								    </Alert>
								</div>

								<div className="flex flex-row justify-center mx-auto gap-4 flex-wrap">
									 {
									 		messages.length > 0 ? messages : 'No messages'
									 }
								</div>
						</Layout>
			)
}

export default Messages