import { DataProvider } from './DataContext.js';
import { Routes, Route, useLocation, useNavigate } from 'react-router-dom';
import { Home, Experience, Education, Skills, Certificates, Settings, Projects, Messages, Landing, Logout, PageNotFound, ConvertImage } from './pages';
import { useState, useEffect } from 'react';
import ProtectedRoutes from './auth/ProtectedRoutes';

const App = () => {
 let storage = localStorage.getItem('token');
 let api_url = `${import.meta.env.VITE_API_URL}/user/user-details`;
 let settings_url = `${import.meta.env.VITE_API_URL}/system-settings/`;
 let location = useLocation();
 let navigate = useNavigate();

 const [data, setData] = useState({
    id: null,
    email: null,
    isAdmin: null
 })

 const [settings, setSettings] = useState({
    name: null,
    shortName: null,
    banner: null
 })

 const unsetData = () => {
   localStorage.clear();
 }
 
 const retrieveData = () => {
   fetch(api_url, {
     method: 'GET',
     headers: {
       Authorization: `Bearer ${storage}`
     }
   })
     .then(result => result.json())
     .then(resp => {
       setData({
         id: resp._id,
         email: resp.email,
         isAdmin: resp.isAdmin
       })
       
     });
 }; 

 const retrieveSettings = () => {
   fetch(settings_url, {
     method: 'GET',
     headers: {
       Authorization: `Bearer ${storage}`
     }
   })
     .then(result => result.json())
     .then(resp => {
       let NAME = resp.find((name) => name.key === 'System Name'); 
       let SHORTNAME = resp.find((sName) => sName.key === 'System Short Name');
       let BANNER = resp.find((banner) => banner.key === 'Landing Banner');

       if (NAME || SHORTNAME || BANNER) {
          setSettings({
            name: NAME.value,
            shortName: SHORTNAME.value,
            banner: BANNER.value
          })
       }

     })
     .catch(error => console.error)
 }; 

 useEffect(() => {
    retrieveData();
    retrieveSettings();
 }, [])

 useEffect(() => {
   const timeoutId = setTimeout(() => navigate('/logout'), 30 * 60 * 1000 );
   return () => clearTimeout(timeoutId); 
 }, [])

  return (
    <>
      <DataProvider value={{ data, setData, unsetData, settings, setSettings }}>
        <Routes key={location.pathname} location={location} >
          <Route index element={<Landing />} />
          
          <Route element={<ProtectedRoutes />}>
            <Route path="/home" element={<Home />} />
            <Route path="/messages" element={<Messages />} />
            <Route path="/education" element={<Education />} />
            <Route path="/experience" element={<Experience />} />
            <Route path="/skills" element={<Skills />} />
            <Route path="/certificates" element={<Certificates />} />
            <Route path="/projects" element={<Projects />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="/tools/image-convert" element={<ConvertImage />} />
          </Route>

          <Route path="/logout" element={<Logout />} />
          <Route path="/*" element={<PageNotFound />} />
        </Routes>
      </DataProvider>
    </>
  )
}

export default App
