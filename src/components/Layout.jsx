import { Card, CardHeader, CardBody, Button, Typography, CardFooter, Progress } from '@material-tailwind/react';
import { NavBar, SideNavBar, Loading, CustomButton } from './';
import { ToastContainer } from 'react-toastify';
import { RiRefreshFill } from "react-icons/ri";
import { useContext } from 'react';
import DataContext from '../DataContext.js';

const Layout = ({ children, childrenClass, loading, refresh, pageTitle, component3, card2, component4, childrenClass2, children2, copyRight, copyRight2, itemNumber, sort }) => {
	
	const { settings } = useContext(DataContext);

		return (
					<>
							<NavBar />	
							<div className="flex flex-row">
									<SideNavBar />
										<Card shadow={false} color="transparent" className="mt-2 mr-2 h-full w-full">
							      <div className="py-1 bg-[#CED7D5] h-full max-h-[3rem] min-h-[3rem] rounded-t-md shadow border-t-[5px] border-t-[#828484] border-r-[3px] border-[#7D9A93] flex flex-row justify-between items-center justify-center">
							      		<Typography className="uppercase font-ibm font-bold tracking-wide text-xl ml-2">{pageTitle}</Typography>      		
							      		<div className="flex flex-row">
							      				<div>{sort}</div>
							      				<CustomButton
							      						icon={<RiRefreshFill className="w-5 h-5" />}
							      						variant="text"
							      						color="green"
							      						handleClick={refresh}
							      						content="refresh"
							      						placement="top"
							      						tooltip="bg-green-600"
							      				/>
							      				{component3}
							      		</div>
							      </div>
							      <CardBody className="h-full p-5 bg-[#DAE4E2] shadow border-r border-r-[3px] border-[#7D9A93] rounded-b-md w-full min-h-[78vh] max-h-[78vh] overflow-y-auto scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100 ">
							        	<div className={childrenClass}>
							        			{children}
							        	</div>
							        	<div className={loading}>
							        			<Loading />
							        	</div>
							      </CardBody>
							      <CardFooter className="py-1 px-0.5 rounded-b-xl flex flex-row justify-between">
							        <div className="font-semibold text-xs uppercase">
							        		{ settings.shortName }
							        </div>
							        <div className="font-semibold text-xs lowercase italic">
							        		{itemNumber}
							        </div>
							        <div className={`font-semibold text-xs uppercase ${copyRight}`}>
							        			&copy; 2023
							        </div>
							      </CardFooter>
							    </Card>

							    			<Card shadow={false} color="transparent" className={`mt-2 mr-3 h-full w-full max-w-[16rem] ${card2}`}>
							          <div className="py-1 bg-[#CED7D5] h-full max-h-[3rem] min-h-[3rem] rounded-t-md shadow border-t-[5px] border-t-[#828484] border-r-[3px] border-[#7D9A93] flex flex-row justify-center">
							          		<div className="flex items-center justify-center font-ibm uppercase">
							          				{component4}
							          		</div>
							          </div>
							          <CardBody className="h-full p-5 bg-[#DAE4E2] shadow border-r border-r-[3px] border-[#7D9A93] rounded-b-md w-full min-h-[78vh] max-h-[78vh] overflow-y-auto scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100">
							            	<div className={childrenClass2}>
							            			{children2}
							            	</div>
							            	{/*<div className={loading}>
							            			<Loading />
							            	</div>*/}
							          </CardBody>
							          <CardFooter className="py-1 px-0.5 rounded-b-xl flex flex-row justify-end">
							            <div className={`font-semibold text-xs uppercase ${copyRight2}`}>
							            			&copy; 2023
							            </div>
							          </CardFooter>
							        </Card>
							</div>
							
							<div className="absolute">
							  <ToastContainer position="bottom-center" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
							</div>	
					</>
		)
}

export default Layout;