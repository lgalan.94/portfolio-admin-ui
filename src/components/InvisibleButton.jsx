import { Tooltip } from '@material-tailwind/react';

const InvisibleButton = ({ handleClick, label, placement, tooltip, icon, btn }) => {
		return (
    <Tooltip 
    		placement={placement}
    		content={label}
    		animate={{
        mount: { scale: 1, y: 0 },
        unmount: { scale: 0, y: 25 },
      }}
      className={`${tooltip} bg-cyan-600 text-xs`}
    >
    		<button onClick={handleClick} className={`flex justify-center items-center invisible shadow mx-auto p-1.5 hover:scale-105 hover:shadow-lg hover:z-10 group-hover:visible bg-gray-300 rounded-full text-cyan-600 ${btn}`}> 
    		    {icon} 
    		</button>
    </Tooltip>
		 )
}

export default InvisibleButton