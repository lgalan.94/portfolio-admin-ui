import { Textarea, Accordion, AccordionHeader, AccordionBody, Input } from '@material-tailwind/react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CustomDialogBox, InvisibleButton, CustomButton, CustomDrawer } from './';
import { RiDeleteBin3Fill } from "react-icons/ri";
import { GiBoltEye } from "react-icons/gi";
import { MdRebaseEdit } from "react-icons/md";
import { useState, useEffect } from 'react';
import { ImSpinner2 } from "react-icons/im";

const SettingsCard = (props) => {
	let { _id, key, value } = props.SettingsProps;
	const [openDialog, setOpenDialog] = useState(false);
	const [title, setTitle] = useState('');
	const [open, setOpen] = useState(0); 
	const [isClicked, setIsClicked] = useState(false);
	const [isUpdateClicked, setIsUpdateClicked] = useState(false);
	const [openDrawer, setOpenDrawer] = useState(false);

	const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
	const handleCloseDrawer = () => setOpenDrawer(false);

	const handleOpenDialog = () => setOpenDialog(!openDialog);	
	const handleOpen = (value) => setOpen(open === value ? 0 : value);
	
	const [newKey, setKey] = useState(key, (prevKey) => prevKey); // Access previous key
	const [newValue, setValue] = useState(value, (prevValue) => prevValue); // Access previous value

	const handleKeyChange = (e) => {
				setKey((prevKey) => e.target.value); 
	}

	const handleValueChange = (e) => {
				setValue((prevValue) => e.target.value); 
	}

 const handleDelete = async (e) => {
					setIsClicked(true);
	    e.preventDefault();
	    try {
	      const response = await fetch(`${import.meta.env.VITE_API_URL}/settings/${_id}`, {
	        method: 'DELETE',
	        headers: {
	          'Content-Type': 'application/json'
	        }
	      });
	      if (response.ok) {
	        toast.success(`${key} Successfully Deleted!`);
	        setOpenDialog(false)
	        setIsClicked(false)
	      } else {
	        toast.error('Delete Error! ');
	        setTimeout(() => 1000);
	        setIsClicked(false)
	      }
	    } catch (error) {
	      alert("unknown error")
	    }
	  };

	 const handleUpdate = (event) => {
	   event.preventDefault();
	   setIsUpdateClicked(true);
	   fetch(`${import.meta.env.VITE_API_URL}/settings/${_id}`, {
	     method: 'PATCH',
	     headers: {
	       'Content-Type': 'application/json',
	     },
	     body: JSON.stringify({
	       newKey,
	       newValue
	     })
	   })
	     .then(response => response.json())
	     .then(data => {
	       if (data === true) {
	         toast.success("Update Successful!");
	         setOpenDrawer(false);
	         setIsUpdateClicked(false);
	       } else {
	         toast.error('Error updating settings!');
	         setTimeout(() => 1000);
	         setIsUpdateClicked(false);
	       }
	     });
	 };

	  useEffect(() => {
	    fetch(`${import.meta.env.VITE_API_URL}/settings/${_id}`)
	      .then((result) => result.json())
	      .then((data) => {
	        setTitle(data.key)
	      });
	  }, [_id]);

			return (
						<>
							<CustomDrawer
									open={openDrawer}
									openDrawer={handleOpenDrawer}
									closeDrawer={handleCloseDrawer}
							>
									<form className="flex self-center flex-row gap-4 p-10">
									  <div className="w-2/5">
									  		<Input
									  				label="KEY" 
									  				value={newKey}
									  				onChange={handleKeyChange}
									  				color="teal"
									  		/>
									  </div>
									  <div className="w-3/5">
									  		<Input
									  				label="VALUE" 
									  				value={newValue}
									  				onChange={handleValueChange}
									  				color="teal"
									  		/>
									  </div>
									</form>
									<div className="flex flex-row mt-4 gap-2 justify-center">
											<CustomButton 
													label="Cancel"
													handleClick={handleCloseDrawer}
											/>
											{
													!isUpdateClicked ? (
																<CustomButton 
																		label="Update"
																		handleClick={handleUpdate}
																		color="green"
																/>
														) : (
																<CustomButton 
																		label="Updating..."
																		icon={<ImSpinner2 className="w-4 h-4 animate-spin" />}
																		color="green"
																		isDisabled={true}
																/>
														)
											}
									</div>
							</CustomDrawer>
			    <Accordion open={open === 1} className="rounded-lg border border-blue-gray-300 w-full w-full max-w-[10rem] group px-4">
         <AccordionHeader
           onClick={() => handleOpen(1)}
           className={`border-b-0 transition-colors font-ibm  ${
             open === 1 ? "text-green-500 hover:!text-green-700" : ""
           }`}
         >
           {key}
           <InvisibleButton
           		icon={<MdRebaseEdit />}
           		label="edit"
           		tooltip="!bg-green-500"
           		placement="top"
           		btn="!text-green-500 absolute right-12"
           		handleClick={handleOpenDrawer}
           />
           <InvisibleButton
           		icon={<RiDeleteBin3Fill />}
           		label="delete"
           		tooltip="!bg-red-500"
           		placement="top"
           		btn="!text-red-500 absolute right-2"
           		handleClick={handleOpenDialog}
           />
         </AccordionHeader>
         <AccordionBody className="pt-0 font-ibm text-sm font-normal">
       					{value}
         </AccordionBody>
       </Accordion>
			    	

			    	<CustomDialogBox
			    			title="Confirmation"
			    			open={openDialog}
			    			handleOpen={handleOpenDialog}
			    	>
			    				Delete {title}?
			    			
			    			<div className="mt-10 flex flex-row justify-center gap-1">
			    					<CustomButton
			    							label="cancel"
			    							handleClick={handleOpenDialog}
			    					/>
			    					{
			    							!isClicked ? (
   													<CustomButton
   															label="delete"
   															color="red"
   															handleClick={handleDelete}
   													/>
			    								):(
			    									<CustomButton
			    											label="deleting..."
			    											color="red"
			    											isDisabled={true}
			    									/>
			    							)
			    					}
			    			</div>

			    </CustomDialogBox>
						</>
			)
}

export default SettingsCard