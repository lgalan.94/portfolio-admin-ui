import { List, ListItem, ListItemPrefix, ListItemSuffix, Typography, Chip, Accordion, AccordionHeader, AccordionBody } from '@material-tailwind/react';
import { NavLink, useLocation } from 'react-router-dom'
import { useState, useEffect } from 'react';
import { SiManageiq } from "react-icons/si";
import { FaUsersGear } from "react-icons/fa6";
import { GoReport } from "react-icons/go";
import { FaChevronDown, FaChevronRight, FaImages } from "react-icons/fa6";
import { FaHome } from "react-icons/fa";
import { IoIosContact } from "react-icons/io";
import { MdSchool } from "react-icons/md";
import { GrUserWorker } from "react-icons/gr";
import { GiSkills } from "react-icons/gi";
import { PiCertificateBold } from "react-icons/pi";
import { MdOutlineSettingsSuggest } from "react-icons/md";
import { GoProjectSymlink } from "react-icons/go";
import { FaMessage } from "react-icons/fa6";

const Navlinks = () => {

	let currentPath = useLocation().pathname;
	let settingsPath = '/ulafis/admin/settings';
	let addSettingsPath = '/ulafis/admin/settings/add';
	let usersPath = '/ulafis/admin/manage-users';

	const [open, setOpen] = useState(0); 
 const handleOpen = (value) => {
   setOpen(open === value ? 0 : value);
 };

	const CustomLink = ({ name, linkIcon, linkPath, className, chipValue, chipClassname, listItem  }) => {

	  return (
	    <NavLink 
	      as={NavLink}
	      to={linkPath}
	      className={`${className} ${currentPath === linkPath ?  'bg-white/50 shadow-sm rounded-lg' : 'bg-none' } `}
	    >
	      <ListItem className="!font-ibm">
	        <ListItemPrefix>
	          {linkIcon}
	        </ListItemPrefix>
	        {name}
	        <ListItemSuffix>
            <span className={`${chipClassname} text-xs bg-gray-100 px-2 py-1 rounded-full`}>{chipValue}</span>
          </ListItemSuffix>
	      </ListItem>
	    </NavLink>
	  )
	}

	const CustomAccordion = ({ children, title, icon, a_value }) => {
				return (
						<Accordion
			     open={open === a_value}
			     icon={
			       <FaChevronDown
			         strokeWidth={2.5}
			         className={`mx-auto h-4 w-4 transition-transform ${open === a_value ? "rotate-180" : ""}`}
			       />
			     }
			   >
			     <ListItem className="p-1" selected={open === a_value}>
			       <AccordionHeader onClick={() => handleOpen(a_value)} className="border-b-0 px-2 py-1">
			         <ListItemPrefix>
			           {icon}
			         </ListItemPrefix>
			         <Typography color="blue-gray" className="font-ibm mr-auto font-normal">
			           {title}
			         </Typography>
			       </AccordionHeader>
			     </ListItem>
			     <AccordionBody className="py-1">
			       <List className="ml-3 p-0">
			         	{children}
			       </List>
			     </AccordionBody>
			   </Accordion>
				)
	}

	return (
		<>
			<List className="font-ibm">
					<CustomLink 
						name="Home" 
						linkIcon={<FaHome className="h-5 w-5" />} 
						linkPath="/home" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Messages" 
						linkIcon={<FaMessage className="h-5 w-5" />} 
						linkPath="/messages" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Education" 
						linkIcon={<MdSchool className="h-5 w-5" />} 
						linkPath="/education" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Work Experience" 
						linkIcon={<GrUserWorker className="h-5 w-5" />} 
						linkPath="/experience" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Skills" 
						linkIcon={<GiSkills className="h-5 w-5" />} 
						linkPath="/skills" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Projects" 
						linkIcon={<GoProjectSymlink className="h-5 w-5" />} 
						linkPath="/projects" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="Certificates" 
						linkIcon={<PiCertificateBold className="h-5 w-5" />} 
						linkPath="/certificates" 
						chipClassname="hidden"
					/>
					<CustomLink 
						name="System Settings" 
						linkIcon={<MdOutlineSettingsSuggest className="h-5 w-5" />} 
						linkPath="/settings" 
						chipClassname="hidden"
					/>
					<hr className="border border-1" />
					<CustomLink 
						name="Image to base64" 
						linkIcon={<FaImages className="h-5 w-5" />} 
						linkPath="/tools/image-convert" 
						chipClassname="hidden"
					/>
			</List>
		</>
	)
}

export default Navlinks