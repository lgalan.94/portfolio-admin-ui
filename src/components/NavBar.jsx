import React from 'react'
import { Navbar, Typography, Button, IconButton, Drawer, Menu, MenuHandler, MenuList, MenuItem } from "@material-tailwind/react";
import { useNavigate } from 'react-router-dom';
import { Navlinks } from './'
import DataContext from '../DataContext.js';
import { useContext } from 'react';
import { FaChevronDown } from "react-icons/fa";
import { HiOutlineLogout } from "react-icons/hi";
 
const NavBar = () => {

  const { data, settings } = useContext(DataContext);
  const [openMenu, setOpenMenu] = React.useState(false);
  const [openNav, setOpenNav] = React.useState(false);
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const openDrawer = () => setOpen(true);
  const closeDrawer = () => setOpen(false);
 
  React.useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpenNav(false),
    );
  }, []);

  React.useEffect(() => { 
      if (data.isAdmin !== null) {
        setIsLoading(false)
      }
  }, [data])

  let userDetails = (
          data.isAdmin ? 'admin' : 'guest'
    )

 
  return (
    <>
    <div className="sticky top-0 z-10 max-h-[768px] w-full ">
      <Navbar className="z-10 h-max max-w-full rounded-none px-4 py-1 lg:px-8 shadow-lg">
        <div className="flex items-center justify-between text-blue-gray-900">
          <Typography
            className="mr-4 cursor-pointer py-1.5"
          >
            <span className="font-bold font-ibm uppercase text-orange-500">{settings.name}</span> 
          </Typography>
          <div className="flex items-center gap-4">
            <div className="flex items-center gap-x-1">
               <Menu open={openMenu} handler={setOpenMenu} allowHover>
                 <MenuHandler>
                   <Button
                     variant="text"
                     className="flex items-center gap-1 text-xs font-semibold uppercase tracking-normal"
                   >
                     { isLoading ? '...' : userDetails } {" "}
                     <FaChevronDown
                       strokeWidth={2.5}
                       className={`h-3.5 w-3.5 transition-transform ${
                         openMenu ? "rotate-180" : ""
                       }`}
                     />
                   </Button>
                 </MenuHandler>
                 <MenuList>
                   <Typography className="text-sm flex flex-row justify-center">Role: &nbsp;<span className="uppercase text-green-500">{data.isAdmin ? "admin" : "guest"} </span> </Typography>
                   <hr className="my-3" />
                   <MenuItem onClick={() => navigate('/logout')}  className="font-ibm flex flex-row items-center justify-center text-xs tracking-wide gap-1 uppercase"><HiOutlineLogout className="w-4 h-4" /> Logout</MenuItem>
                 </MenuList>
               </Menu>
            </div>
            <IconButton
              variant="text"
              className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
              ripple={false}
              onClick={openDrawer}
            >
              {openNav ? (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  className="h-6 w-6"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-6 w-6"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M4 6h16M4 12h16M4 18h16"
                  />
                </svg>
              )}
            </IconButton>
          </div>
        </div>
        
      </Navbar>
    </div>

    <React.Fragment>
      <Drawer placement="right" open={open} onClose={closeDrawer}>
        <div className="mb-2 flex capitalize items-center justify-between p-4">
          <IconButton variant="text" color="blue-gray" onClick={closeDrawer}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={2}
              stroke="currentColor"
              className="h-5 w-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </IconButton>
        </div>
        
        <Navlinks />
        
      </Drawer>
    </React.Fragment>

    </>

  );
}

export default NavBar