import { ImSpinner2 } from "react-icons/im";

const Loading = ({ className }) => {
  return (
    <div className={`flex items-center justify-center h-full min-h-[82vh] md:min-h-[85vh] lg:min-h-[71vh]`}>
      <div className="flex flex-col p-5 rounded-lg gap-1 items-center justify-center">
        <ImSpinner2 className="animate-spin spin 10s w-20 h-20" /> 
        <div className="tracking-wider"> Loading... </div>
      </div>
    </div>
  );
};
 
export default Loading;