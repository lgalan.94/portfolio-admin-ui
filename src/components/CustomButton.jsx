import { Button, Tooltip } from '@material-tailwind/react';
import { ImSpinner2 } from "react-icons/im";

const CustomButton = ({ label, variant, color, isDisabled, icon, handleClick, btn, content, placement, tooltip, type }) => {
		return (
					<Tooltip className={`${tooltip} text-xs`} content={content} placement={placement}>
							<Button type={type} shadow={false} onClick={handleClick} className={`${btn} hover:scale-105 py-2 flex flex-row justify-center items-center gap-0.5`} disabled={isDisabled} size="sm" variant={variant} color={color}>
							  {icon} <span className="text-xs"> {label} </span>
							</Button>
					</Tooltip>
		)
}

export default CustomButton
