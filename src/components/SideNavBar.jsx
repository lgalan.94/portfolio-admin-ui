import { Card, Typography } from "@material-tailwind/react";
import { Navlinks } from './';
import { useNavigate } from 'react-router-dom';
 
export default function SideNavBar() {

  const navigate = useNavigate();
 
  return (
  <>
    <Card shadow={false} variant="gradient" color="transparent" className={`h-full  w-full max-w-[18rem] p-1 rounded-none`}>
      <div className="mb-2 capitalize p-4">
        <Typography variant="h5" color="blue-gray">
          <Navlinks />
        </Typography>
      </div>
    </Card>
  </>
      
  );
}