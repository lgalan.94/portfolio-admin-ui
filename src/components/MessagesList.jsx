import { RiDeleteBin3Fill } from "react-icons/ri";
import { InvisibleButton } from './'

const MessagesList = ({ name }) => {
			return (
						<div className="text-center px-6 pt-3 uppercase border border-1 border-dark/25 rounded-md group hover:scale-105 shadow-lg"> 
									<span>{name}</span>
									<InvisibleButton
											icon={<RiDeleteBin3Fill />}
											label="delete"
											tooltip="!bg-red-500"
											placement="bottom"
											btn="!text-red-500"
											btnClas
									/>
						</div>
			)
}

export default MessagesList