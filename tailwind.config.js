const withMT = require("@material-tailwind/react/utils/withMT");
 
module.exports = withMT({
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
      extend: {
        fontFamily: {
          ibm: ['IBM Plex Sans', 'sans'],
        },
        colors: {
          defaultColor: '#B9D3C0',
        },
      },
    },
  plugins: [
      require('tailwind-scrollbar'),
  ],
});